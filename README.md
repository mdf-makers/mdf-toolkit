# Merged Data File (MDF) "Open Data Toolkit"

The YOPP observation siteMIP (model inter-comparison program) was convened to facilitate observational super sites and NWP modelers making their high resolution in-space-and-time data available in a cross-compatible standard formatted way, specifically for process evaluation. The project has grown in somewhat in scope to be a more generic data formatting toolkit. It is a python project lead by Michael Gallagher (CIRES/NOAA Research Scientst), co-maintained with significant contributions by Jareth Holt (MISU), designed to simplify processing data into this standardized format, for scientists working with site observationsand model output files. The idea being that the toolkit provides an interface between your data and the details of the netcdf output format, saving you time, energy, and effort. As a user of the toolkit writing a simple basic well-formatted fully documented netcdf data file could be as simple as seven lines of code:

~~~python
from mdftoolkit.MDF_toolkit import MDF_toolkit

import pandas as pd
MDF     = MDF_toolkit(supersite_name='DEMO', multi_site=False)
header  = ['albs','rsu','rsd','ts','hur','ta','tdp','ps','rlu']
ts_data = pd.read_csv('my_data.csv', sep=',', names=header)

MDF.add_data_timeseries(ts_data, cadence="time01")
MDF.write_files(output_dir='./out/')

~~~
 
The MDF toolkit is a python library/API that provides the containers and functions that make writing merged data files easier for data maintainers for complex sites and model files. This includes codified definitions and attributes of the M(O/M)DF specification as well as methods and functions to write your data to disk in this specification. The essential code can be found in <code>src/mdftoolkit/MDF_toolkit.py</code> and the data definitions provided by the venerable H-K data documentation table can be found in <code>src/mdftoolkit/HKTable/</code>

<code>src/mdftoolkit/examples/create_example_MDF.py</code> provides a skeleton example on which you can base your site data processing code. It provides a (semi) detailed example of how to utilize the functionality provided by the MDF_toolkit. This code generates some random fake data files and inserts these data into an MDF using the MDF_toolkit.

## Installing and getting started:

The long term plan is to submit this package to the PyPI repository so using it is as simple as "pip install mdftoolkit". For now, please read the more detailed instructions in INSTALL.md

### Where to find help and collaborate:

A slack channel was created for general discussion of the toolkit as a place to collaborate together. This is the ideal place to work on the nitty gritty details. You can join the slack channel at:

<http://mdfmakers.slack.com/>

## Code description: 

A general description of the steps to use the toolkit:

1) ingest "raw" data sources from observatory via pandas/xarray
2) QC/curate your data as appropriate, ideally inside MDF toolkit for documentation/provenance purposes
3) label the data appropriately via provided toolkit functions so it knows how to read/write your data 
4) ask the toolkit to write these data objects to disk !

The difficulty of these steps will highly depend on the format of your data. It could be anything from a few lines of code per data source to a few 100 lines of code per data source, largely depending on how much you need to clean and format your data. 

### Required software:

If you would like to create an MDF, the following python packages are required:

~~~
python  ≥ 3.6
netCDF4 ≥ 1.3.0
numpy   ≥ 1.13.0
pandas  ≥ 0.20
~~~

## Linux setup:

### Method 1: 

For Linux, setup is as simple as using your package manager to install the necessary packages

Using Ubuntu as an example you would run the following commands:

```
sudo apt-get install python-numpy python-pandas python-netcdf 
```

Or on Arch: 

```
pacman -S python python-numpy python-scipy python-netcdf4 
```

### Method 2:
If you are running on a server and you don't have the permissions to install python packages but you have Pip, you can install these Python packages locally in your home directory. Double check to make sure they aren't installed to root already but if you don't have them, run:
```
pip install --user numpy pandas netCDF4
```

And then add the following to your ~/.bashrc file:

 ```
export PY_USER_BIN=$(python -c 'import site; print(site.USER_BASE + "/bin")')
export PATH=$PY_USER_BIN:$PATH
```

## Windows/OSX setup:

I'm a *NIX guy myself but you most likely want to use [Anaconda](https://docs.anaconda.com/anaconda/user-guide/getting-started/). Using Anaconda, the necessary packages can be installed with the following command:
~~~
conda install -c pandas netcdf4 numpy xarray
~~~

## Want to contribute?

Great, I would love your contributions! Check this repository out, make your modification, and send me a pull request here. Or, worst case scenario feel free to open up an "issue" here on gitlab with your comments. 

If git is annoying and you would like to contact me instead by e-mail, I'm happy to work something out. Don't hesitate to contact me, [Michael Gallagher](mailto:michael.r.gallagher@noaa.gov). 

To use this code, modify it, and give that mdfication back to me, follow this basic recipe:
```
git clone https://gitlab.com/mdf-makers/mdf-toolkit.git  # <!-- checkout this repo via https OR  -->
git clone git@gitlab.com:mdf-makers/mdf-toolkit.git  # <!-- checkout this repo via ssh  -->
git checkout -b supersitename-mdf # <!-- make a new branch for your site  -->
```
Then, make changes inside of your branch and commit them as necessary upstream to the MDF toolkit repo!

## supersite contacts:
 
MDFs are going to be created by teams for each supersite observatory. Here we've collected contact point names for each observatory as a convenient starting point for site specific discussions/questions:

* MOSAiC — <michael.r.gallagher@noaa.gov>

## Acknowledgments

Code contributors:
* Michael Gallagher — chief toolkit architect/maintainer — <michael.r.gallagher@noaa.gov>
* Jareth Holt — contributed code for table processing  — <jareth.holt@misu.su.se>

Data-specification wranglers:
* Leslie Hartten — <leslie.m.hartten@noaa.gov>
* Siri Jodha Khalsa — <sjsk@nsidc.org>

YOPP siteMIP steerers/evangelists:
* Taneil Uttal — MDF evangelist — <taneil.uttal@noaa.gov>
* Gunilla Svensson — siteMIP evangelist — <gunilla@misu.su.se>

And the many more who have put time and energy into the YOPP siteMIP project
