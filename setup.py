#!/usr/bin/env python
"""Setup script for mdf-toolkit.
"""

# Import statements
import setuptools
from setuptools.config import read_configuration

# Read setup.cfg and make a dictionary of all options
conf_dict = read_configuration('setup.cfg')
full_dict = dict()
for subdict in conf_dict.values():
    full_dict.update(subdict)

# Fix any Specifiers to be read as strings
for (key, val) in full_dict.items():
    if isinstance(val, setuptools.extern.packaging.specifiers.SpecifierSet):
        full_dict[key] = str(val)

# Call the original setup command
setuptools.setup(**full_dict)
