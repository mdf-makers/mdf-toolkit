"""Module defining the attributes and variables of the MDF specification.
"""

import json
import pathlib
import sys
if sys.version_info >= (3, 9):
    import importlib.resources as resources
else:
    import importlib_resources as resources

# List of the HK tables
TABLES = ('Global', 'Instructions', 'Lineage', 'Variable')

# Map from new keys to current keys in attributes files
GLOBATTRMAP = dict(
    name='Attribute_fromACDD_CF_OrISO_TC211_',
    desc='DescriptionOfRequiredInformation',
    mdf='MMDF_MODF_', notes='Notes', phase='DevelopmentPhase')
VBLATTRMAP = dict(
    name='VariableName', long_name='long_name', delta_t='delta_t',
    standard_name='standard_name', units='units', calendar='calendar',
    mdf='MMDF_MODF_', phase='DevelopmentPhase', notes='Notes',
    uri='URIForVariableDefinition',
    minattrs='MinimumRecommendedVariableAttributes_ifApplicable_')


# Helper functions
def _format_lineage(fp_lineage):
    """Read and reformat the Lineage file.
    """
    # Read the file
    with open(fp_lineage, mode='rt') as fp:
        data = json.load(fp)

    # Extract the version information
    hktableversion = data['Version'][0]
    data = data['DocLineage']

    # Reformat the lineage, organizing into sections
    lineage = list()
    for line in data:
        # Is this a new section?
        v1, v2 = line['Var1'], line['Var2']
        if v2 != '':
            lineage.append([': '.join([v1, v2])])
        else:
            lineage[-1].append(v1)
    return (hktableversion, lineage)


def _format_instructions(fp_inst):
    """Read and reformat the Instructions file.
    """
    # Read the file
    with open(fp_inst, mode='rt') as fp:
        data = json.load(fp)

    # Extract the version information
    hktableversion = data['Version'][0]
    data = data['DocInstructions']

    # Reformat the instructions, organizing into sections
    i = 0
    inst = list()
    while i < len(data):
        line = data[i]['Var1']
        # Is this a new section?
        if len(inst) == 0:
            inst.append([line])
        elif line == '':
            # Include next line as new section
            i += 1
            inst.append([data[i]['Var1']])
        elif line.startswith('end of H-K_Table'):
            pass
        else:
            # Append to the current section
            inst[-1].append(line)
        i += 1
    return (hktableversion, inst)


def _format_global(fp_global):
    """Read and reformat the Global attributes file.
    """
    # Read the file
    with open(fp_global, mode='rt') as fp:
        data = json.load(fp)

    # Extract the version information
    keys = list(data)
    hktableversion = data['Version'][0]
    data = data[keys[1]]

    # Reformat the attributes, renaming some keys for convenience
    globattrs = dict()
    for line in data:
        line = {
            newkey: line[oldkey]
            for (newkey, oldkey) in GLOBATTRMAP.items()}
        name = line.pop('name')
        globattrs[name] = line
    return (hktableversion, globattrs)


def _format_variable(fp_variable):
    """Read and reformat the Variable attributes file.
    """
    # Read the file
    with open(fp_variable, mode='rt') as fp:
        data = json.load(fp)

    # Extract the version information
    keys = list(data)
    hktableversion = data['Version'][0]
    data = data[keys[1]]

    # Reformat the attributes, renaming some keys for convenience
    # Sections of the variable table are currently ignored;
    # the code could be rewritten to keep track of section if needed
    vblattrs = dict()
    for line in data:
        # Is this a section title or header?
        if (
            line['long_name'] in ('', 'long_name')
            or 'variable name' in line['VariableName'].lower()
        ):
            continue

        line = {
            newkey: line[oldkey]
            for (newkey, oldkey) in VBLATTRMAP.items() if oldkey in line}

        name = line.pop('name')
        # Split minimum attributes into a list
        line['minattrs'] = line['minattrs'].split('; ')
        vblattrs[name] = line
    return (hktableversion, vblattrs)


# Primary functions
def findjsonfiles(hktabledir=None, hktableversion=None):
    """Find the JSON files used to construct the definitions.

    Find the JSON files for the HK table. The most recent standard files have
    been included in mdf-toolkit/src/mdftoolkit/HKTable. An alternative
    directory for the files and a specific version can be provided. The
    filenames are then:

        [hktabledir]/HK_[table].[hktableversion].json

    where "hktabledir" is the directory, "table" is one of the tables (Global,
    Instructions, Lineage, or Variable), and "hktableversion" is a version
    specifier.

    Parameters
    ----------
    hktabledir: str or pathlib.Path, optional
        The base directory for the HK table files. If None (default) then
        mdftoolkit/HKTable is used.
    hktableversion: str, optional
        The table version to use. This can be a string such as "1.1", in which
        case the filenames are "[file].V1_1.json". It can also be a literal
        string like "V1_1" or "custom". If None (default) then the latest
        version available in hktabledir is used.

    Returns
    -------
    hkfiles: dict[str, pathlib.Path]
        A dictionary mapping the table name (Global, Variable, etc.) to its
        file path.
    """
    # Determine directory to search in
    if hktabledir is None:
        hktabledir = resources.files('mdftoolkit') / 'HKTable'
    else:
        hktabledir = pathlib.Path(hktabledir)
    avail = list(hktabledir.glob('HK_*.*.json'))
    if len(avail) == 0:
        raise RuntimeError(
            f'There are no files in directory {hktabledir=!s} with the format '
            '"HK_*.*.json"')

    # Determine version to use (if multiple)
    if hktableversion is None:
        # Find the latest version
        vstrs = set('.'.join(fp.name.split('.')[1:-1]) for fp in avail)
        if len(vstrs) == 1:
            # Only one available version
            hktableversion = list(vstrs)[0]
        else:
            vnums = [
                tuple(int(val) for val in vstr[1:].split('_'))
                for vstr in vstrs if vstr.startswith('V')]
            if len(vnums) == 0:
                raise RuntimeError(
                    f'None of the files in directory {hktabledir=!s} have '
                    'version number suffixes like "V1_1". Try specifying a '
                    'specific version suffix.')
            vnum = max(vnums)
            hktableversion = 'V' + '_'.join(str(val) for val in vnum)
    elif all(val.isnumeric() for val in hktableversion.split('.')):
        # Transform version number into version string
        hktableversion = 'V' + '_'.join(hktableversion.split('.'))

    # Extract file paths
    hkfiles = {
        table: hktabledir / f'HK_{table}.{hktableversion}.json'
        for table in TABLES}
    missing = [fp.name for fp in hkfiles.values() if not fp.exists()]
    if len(missing) > 0:
        raise RuntimeError(
            f'The following expected files are missing from {hktabledir=!r}: '
            + ', '.join(missing))
    return hkfiles


def define_mdf_variables(hktabledir=None, hktableversion=None):
    """Define the standard variables in the MDF specification.
    """
    hkfiles = findjsonfiles(
        hktabledir=hktabledir, hktableversion=hktableversion)
    __, mdf_vars = _format_variable(hkfiles['Variable'])
    return mdf_vars


def define_mdf_globals(hktabledir=None, hktableversion=None):
    """Define the global attributes of the MDF specification.
    """
    # written with double quotes so that it's not pulled in by the
    # change_var script
    hkfiles = findjsonfiles(
        hktabledir=hktabledir, hktableversion=hktableversion)
    __, global_atts = _format_global(hkfiles['Global'])
    return global_atts


def define_mdf_all(hktabledir=None, hktableversion=None):
    """Get the HK table version, global, and variable attributes.
    """
    hkfiles = findjsonfiles(
        hktabledir=hktabledir, hktableversion=hktableversion)
    hkver, global_atts = _format_global(hkfiles['Global'])
    hkver = hkver['Version'][len('Version'):]
    hkver = hktableversion or hkver
    __, mdf_vars = _format_variable(hkfiles['Variable'])
    return (hkver, global_atts, mdf_vars)


# Infer the HK table name from the variable name
def infermdfname(var_name, mdfvars):
    """Infer the MDF variable from the variable's name.

    Determine which variable in the HK table this variable corresponds to based
    on its name. Valid names should start with the HK table name and use
    underscores '_' to add modifiers. Since some variables in the table have
    underscores as well, this function checks for the longest match between the
    variable's name and the HK table variables.

    Parameters
    ----------
    var_name: str
        Name of the variable to check.
    mdfvars: dict[str] or list[str]
        The dictionary of HK table variables being used, as returned by
        define_mdf_variables or define_mdf_all. This could also be the list of
        dictionary keys (i.e. the list of recognized variable names) instead.

    Returns
    -------
    mdfname: str or None
        Name of the variable in the HK table, or None if no match is found.
    """
    # Split the variable into components
    cmps = var_name.split('_')
    n = len(cmps)
    mdfname = None
    for i in range(n, 0, -1):
        # Check for a match to the first i components
        name = '_'.join(cmps[:i])
        if name in mdfvars:
            mdfname = name
    return mdfname
