"""Module for checking MDFs.
"""

# Import statements
import inspect
import sys
import textwrap
import cfunits
import netCDF4
from . import MDF_definitions
if sys.version_info >= (3, 8):
    import importlib.metadata as metadata
else:
    import importlib_metadata as metadata

# Constants
LWIDTH = 80
WRAPPER = textwrap.TextWrapper(
    width=LWIDTH, initial_indent='- ', subsequent_indent='  ')

# Get HK table info
__version__ = metadata.version('mdftoolkit')
HKVER, MDFGLOBS, MDFVARS = None, None, None


# Get the HK table info
def loadhktable(hktabledir=None, hktableversion=None):
    """Load the HK tables into global variables.

    Load the HK tables into the module-level variables HKVER, MDFGLOBS, and
    MDFVARS. Call this function first to run checks with HK tables other than
    the most recent version in mdftoolkit/HKTable.

    Parameters
    ----------
    hktabledir: str or pathlib.Path, optional
        The base directory for the HK table files. If None (default) then
        mdftoolkit/HKTable is used.
    hktableversion: str, optional
        The table version to use. This can be a string such as "1.1", in which
        case the filenames are "[file].V1_1.json". It can also be a literal
        string like "V1_1" or "custom". If None (default) then the latest
        version available in basedir is used.
    """
    global HKVER, MDFGLOBS, MDFVARS
    HKVER, MDFGLOBS, MDFVARS = MDF_definitions.define_mdf_all(
        hktabledir=hktabledir, hktableversion=hktableversion)


loadhktable()


# Primary classes
class Result:
    """Holds the result of a check.

    Structure storing information about an individual check. These structures
    are then used when compiling the final summary report.
    This class is based largely on the Result class of compliance-checker:
    https://github.com/ioos/compliance-checker/

    Attributes
    ----------
    msgs: dict[int, list of str]
        The messages associated with errors of each severity: 3=major,
        2=medium, 1=minor. A check will be considered a failure if there are
        messages associated with it.
    fun: str
        The name of the checking function generating this result, without the
        'check_' prefix.
    """
    WEIGHTS = tuple(range(3, 0, -1))
    WNAMES = ('major', 'medium', 'minor')

    def __init__(self):
        self.msgs = {weight: list() for weight in Result.WEIGHTS}
        fun = inspect.currentframe().f_back.f_code.co_name
        self.fun = fun[len('check_'):]

    def __repr__(self):
        """Complete representation of Result.
        """
        return f'Result(msgs={self.msgs}, fun={self.fun})'

    def __str__(self):
        """Compact representation of Result.
        """
        return (
            f'Result(fun={self.fun}, msgs=['
            + ', '.join(
                f'{len(ms)} {name}'
                for (name, ms) in zip(Result.WNAMES, self.msgs))
            + '])')

    def __bool__(self):
        """Determine if the test was failed (a non-trivial Result).
        """
        return any(len(ms) > 0 for ms in self.msgs.values())


# Individual check functions
def check_haveallglobalattrs(globattrs):
    """Check whether all MDF global attributes are present.

    Given a dictionary of global attributes, check that all relevant global
    attributes from the MDF specification are present. As extra security, this
    function also checks that the attribute values do not match the
    descriptions in the HK table.

    Parameters
    ----------
    globattrs: dict
        The global attributes for this MDF.

    Returns
    -------
    result: Result
        The result of this check.
    """
    result = Result()

    # Get the names of all MDF-specific global attributes
    shouldhave = set(
        name for (name, attr) in MDFGLOBS.items() if attr['mdf'] != 'MMDF')
    missing = shouldhave - set(globattrs)
    if len(missing) > 0:
        result.msgs[3].append(
            'The following MDF global attributes are missing: '
            + ', '.join(missing))

    # Check that the values are different from those of the HK table
    present = shouldhave & set(globattrs)
    unmodified = set(
        name for name in present if globattrs[name] == MDFGLOBS[name])
    if len(unmodified) > 0:
        result.msgs[3].append(
            'The following attributes have the same value as the description '
            'in the HK table, indicating that they have been copied without '
            'modification: ' + ', '.join(unmodified))

    # Check that the values are not empty
    blank = [name for (name, attr) in globattrs.items() if attr == '']
    if len(blank) > 0:
        result.msgs[3].append(
            'The following attributes have empty strings as values: '
            + ', '.join(blank))
    return result


def check_variableinhktable(vblname, mdfname=None):
    """Check whether the given variable is in the HK table.

    Check whether this variable is in the HK table. If the MDF name has been
    given explicitly as an attribute, it can be provided here; otherwise
    infermdfname is used to infer the correct variable in the table.

    Parameters
    ----------
    vblname: str
        Name of the variable to check.
    mdfname: str, optional
        If known, the specific name of the variable in the HK table.

    Returns
    -------
    result: Result
        The result of this check.
    """
    result = Result()
    mdfname1 = mdfname or MDF_definitions.infermdfname(vblname, MDFVARS)
    if mdfname1 is None:
        msg = f'Variable {vblname} '
        if mdfname is not None:
            msg += f'(with {mdfname=} given) '
        msg += 'could not be found in the HK table'
        result.msgs[2].append(msg)
    return result


def check_variableunits(mdfname, vblattrs, vblname=None):
    """Check that the variable units match.

    Check that this variable's units match those of the HK table. cfunits is
    used to check that the units are valid, and whether time units are given a
    recognizable reference time. This module also checks if units are:
    - equal: The units are formally equal, e.g. Pa and N m-2; or
    - equivalent: The units are convertible to each other, e.g. Pa and hPa or K
      and degree_celsius.

    Parameters
    ----------
    mdfname: str
        Name of this variable in the HK table. This function expects the
        variable to have been checked for membership already.
    vblattrs: dict
        The attributes of this variable.
    vblname: str, optional
        The name of this variable to be used in any error messages; if None
        (default) then mdfname will be used.

    Returns
    -------
    result: Result
        The results of this check.
    """
    result = Result()
    mdfattrs = MDFVARS.get(mdfname, None)
    if mdfattrs is None:
        # Assume membership has been checked elsewhere
        return result
    vblname = vblname or mdfname

    mdfval = mdfattrs.get('units', '')
    vblval = vblattrs.get('units', '')

    # Hack to fix unspecific time units
    if 'since' in mdfval:
        dtunits, __, *rest = mdfval.split()
        if rest[0] == '...':
            # Replace with an actual time
            mdfval = f'{dtunits} since 2018-01-01T00:00:00'

    # If the values match, move on!
    if mdfval == vblval:
        return result

    # Is either dictionary missing this attribute?
    if mdfval is None:
        if vblval is not None:
            result.msgs[3].append(
                f'Variable {mdfname} in the HK table has no units '
                f'attribute, but variable {vblname} has units="{vblval}"')
        return result
    elif vblval is None:
        result.msgs[3].append(
            f'Variable {vblname} has no units attribute, but variable '
            f'{mdfname} in the HK table as units="{mdfval}"')
        return result

    # Check if the given units are valid
    vblunits = cfunits.Units(vblval)
    if not vblunits.isvalid:
        result.msgs[3].append(
            f'The units "{vblval}" of variable {vblname} are invalid '
            f'according to cfunits (reason: {vblunits.reason_notvalid})')
        return result

    # Check time units first
    if mdfval.endswith(' since'):
        if not vblunits.isreftime:
            result.msgs[3].append(
                f'The units "{vblval}" of time variable {vblname} cannot '
                'be interpreted by cfunits as a reftime; they should be '
                'in the format "minutes since (ISO datetime)"')
        elif mdfval.split()[0] != vblval.split()[0]:
            result.msgs[1].append(
                f'The units of time variable {vblname} are '
                f'{vblval.split()[0]}; the HK table recommends using '
                f'{mdfval.split()[0]}')
        return result

    # Now check non-time units
    mdfunits = cfunits.Units(mdfval)
    if mdfunits.equals(vblunits):
        result.msgs[1].append(
            f'The units {vblval} of variable {vblname} are equal to the '
            f'units {mdfval} of HK table variable {mdfname}, but it is '
            'still recommended to use the exact unit string')
    elif mdfunits.equivalent(vblunits):
        result.msgs[2].append(
            f'The units {vblval} of variable {vblname} are convertible to '
            f'the units {mdfval} of HK table variable {mdfname} but not '
            'equal to them; it is highly recommended to convert these '
            'units')
    else:
        result.msgs[3].append(
            f'The units {vblval} of variable {vblname} cannot be '
            f'converted by cfunits to the units {mdfval} of HK table '
            f'variable {mdfname}')
    return result


def check_variablecoreattrs(mdfname, vblattrs, vblname=None):
    """Check that the variable has all core attributes.

    Check that this variable has all required core attributes besides units:
    long_name, standard_name, and calendar (for time variables). Also checks
    that the attribute values match, allowing for the long_name to differ.

    Parameters
    ----------
    mdfname: str
        Name of this variable in the HK table. This function expects the
        variable to have been checked for membership already.
    vblattrs: dict
        The attributes of this variable.
    vblname: str, optional
        The name of this variable to be used in any error messages; if None
        (default) then mdfname will be used.

    Returns
    -------
    result: Result
        The results of this check.
    """
    COREATTRS = ('long_name', 'standard_name', 'calendar')
    result = Result()
    mdfattrs = MDFVARS.get(mdfname, None)
    if mdfattrs is None:
        # Assume membership has been checked elsewhere
        return result
    vblname = vblname or mdfname

    for attr in COREATTRS:
        mdfval = mdfattrs.get(attr, '')
        vblval = vblattrs.get(attr, '')
        # If the values match, move on!
        if mdfval == vblval:
            continue

        # Is either dictionary missing this attribute?
        if mdfval is None:
            if vblval is not None:
                result.msgs[3].append(
                    f'Variable {mdfname} in the HK table has no {attr} '
                    f'attribute, but variable {vblname} has {attr}="{vblval}"')
            continue
        elif vblval is None:
            result.msgs[3].append(
                f'Variable {vblname} has no {attr} attribute, but variable '
                f'{mdfname} in the HK table as {attr}="{mdfval}"')
            continue

        # Not matching long_name is minor
        if attr == 'long_name':
            result.msgs[1].append(
                f'Variable {vblname} has '
                + (f'no {attr} ' if vblval == '' else f'{attr}="{vblval}" ')
                + f'but variable {mdfname} in the HK table has '
                + (f'no {attr}' if mdfval == '' else f'"{mdfval}"'))
            continue

        # Not matching standard_name is severe
        if attr == 'standard_name':
            result.msgs[3].append(
                f'Variable {vblname} with {attr}={vblval} does not match the '
                f'HK table {mdfname} value of {mdfval}')
            continue

        # Not matching calendar is minor
        if attr == 'calendar':
            result.msgs[1].append(
                f'Time variable {vblname} has '
                + (f'no {attr}' if vblval == '' else f'{attr}={vblval}')
                + f'; it is recommended to always use {attr}={mdfval}')
            continue
    return result


def check_variableaddattrs(mdfname, vblattrs, vblname=None, globattrs=None):
    """Check that the variable has recommended additional attributes.

    Check that the given variable has all the recommended additional
    attributes. Some of these are okay to have as global attributes instead,
    and are assumed to be shared among all variables.

    Parameters
    ----------
    mdfname: str
        Name of this variable in the HK table. This function expects the
        variable to have been checked for membership already.
    vblattrs: dict
        The attributes of this variable.
    vblname: str, optional
        The name of this variable to be used in any error messages; if None
        (default) then mdfname will be used.
    globattrs: dict, optional
        The global attributes of the dataset. This can include the source,
        references, contributor_name, contributor_email, creator_name,
        creator_email, and institution if such attributes are the same for all
        variables in the file.

    Returns
    -------
    result: Result
        The results of this check.
    """
    GLOBOK = {  # Which attributes are okay as global attributes?
        'source', 'references', 'contributor_name', 'contributor_email',
        'creator_name', 'creator_email', 'institution'}
    SEVERE = {  # Which missing attributes are major?
        'references', 'contributor_name', 'contributor_email', 'institution'}
    IGNORE = {'long_name', 'standard_name', 'units', 'calendar', 'comment'}
    result = Result()
    mdfattrs = MDFVARS.get(mdfname, None)
    if mdfattrs is None:
        # Assume membership has been checked elsewhere
        return result
    vblname = vblname or mdfname
    globattrs = globattrs or dict()

    # Determine which additional attributes should be checked
    mdfattrs = set(mdfattrs['minattrs']) - IGNORE
    mdfattrs -= (GLOBOK & set(globattrs))
    missing = mdfattrs - set(vblattrs)
    major = missing & SEVERE
    minor = missing - SEVERE
    if len(major) > 0:
        result.msgs[3].append(
            f'The variable {vblname} does not have the following attributes '
            'that are necessary for proper attribution: ' + ', '.join(major))
    if len(minor) > 0:
        result.msgs[2].append(
            f'The variable {vblname} does not have the following attributes '
            f'recommended for HK table variable {mdfname}: '
            + ', '.join(minor))
    return result


# Functions to check entire files
def rundataset(ds):
    """Run the checkers on an open netCDF dataset.

    Run all available checks on an open netCDF file.

    Parameters
    ----------
    ds: netCDF4.Dataset
        An open, read-only netCDF file. Global attributes, variable names, and
        variable attributes will be accessed but no data.

    Returns
    -------
    results: list of Result
        The results of all of the checks run on this file.
    """
    results = list()

    # Get and evaluate the global attributes
    globattrs = {attr: ds.getncattr(attr) for attr in ds.ncattrs()}
    results.append(check_haveallglobalattrs(globattrs))

    # Iterate through the variables, applying appropriate checks
    vbls = list(ds.variables)
    for vblname in vbls:
        vblattrs = {
            attr: ds.variables[vblname].getncattr(attr)
            for attr in ds.variables[vblname].ncattrs()}
        # Try to determine mdfname
        mdfname = (
            vblattrs.get('mdf_name', None)
            or MDF_definitions.infermdfname(vblname, MDFVARS))

        # Check if part of HK table
        if not vblname.startswith('time'):
            result = check_variableinhktable(vblname, mdfname=mdfname)
            results.append(result)
            if result:
                # Not in HK table, can't check anything else
                continue
        else:
            # Change mdfname to vblname, since time variables not in HK table
            mdfname = vblname

        results.append(check_variablecoreattrs(
            mdfname, vblattrs, vblname=vblname))
        results.append(check_variableunits(
            mdfname, vblattrs, vblname=vblname))
        results.append(check_variableaddattrs(
            mdfname, vblattrs, vblname=vblname, globattrs=globattrs))
    return results


# Functions to compile reports
def compilereport(results):
    """Compile multiple results into a report.

    Compile the results from several different checks into a report. The error
    messages are grouped according to their severity and then formatted using
    WRAPPER. The output also includes section headers, whitespace, and a
    summary with the total number of errors of each type.

    This function returns a list of strings (without newline characters) to be
    printed or written to file. A simple command to view the compiled report is

        print('\n'.join(report))

    Parameters
    ----------
    results: list of Result
        Result instances from multiple check functions.

    Returns
    -------
    report: list of str
        A list of lines to be printed or written to file.
    """
    # Combine the results into a single dictionary
    result = {weight: list() for weight in Result.WEIGHTS}
    for res in results:
        for (weight, ms) in res.msgs.items():
            result[weight] += ms

    # Print a summary line
    report = list()
    report.append('Total errors: ' + ', '.join(
        f'{len(msgs)} {name}'
        for (name, msgs) in zip(Result.WNAMES, result.values())))
    report.append('')
    # If no errors, great!
    if not any(len(msgs) > 0 for msgs in result.values()):
        report.append('No errors found!')
        return report

    # Add each non-empty section to the report
    for (wname, (weight, msgs)) in zip(Result.WNAMES, result.items()):
        if len(msgs) == 0:
            continue
        report.append('')
        line = f'{wname.capitalize()} errors'
        report.append(line)
        report.append('-'*len(line))
        for msg in msgs:
            report += WRAPPER.wrap(msg)
    return report


# Main function
def main(ncfpath, noprint=False):
    """Open, check, and report on a netCDF file.

    Open the given netCDF file, run all available checks on it, and compile a
    report.

    Parameters
    ----------
    ncfpath: str
        Path to the netCDF file to check.
    noprint: bool, default False
        Disable printing the report; useful if calling from a REPL.

    Returns
    -------
    results: list of Result
        The Result instances generated by each checking function.
    report: list of str
        The individual formatted lines of the report.
    """
    # Make a header for the report
    report = [
        'MDF_checker.py',
        'Check netCDF files for MDF compliance',
        f'MDF_toolkit version {__version__}',
        f'HK table version {HKVER}',
        f'Filepath: {ncfpath}',
        '']

    # Run the checks on the file
    with netCDF4.Dataset(ncfpath, mode='r') as ds:
        results = rundataset(ds)
    report += compilereport(results)

    # Print the report unless explicitly disabled
    if not noprint:
        print('\n'.join(report))
    return (results, report)


# Command-line interface
if __name__ == '__main__':
    # Set up CLI options
    import argparse
    parser = argparse.ArgumentParser(
        description='Check a netCDF file for MDF compliance')
    parser.add_argument('ncfpath', help='Path to the file to check')
    parser.add_argument('--hktabledir', help=(
        'Path to a directory with HK tables (default mdftoolkit/HKTable)'))
    parser.add_argument('--hktableversion', help=(
        'Version of the HK tables to use; defaults to the most recent version '
        'in the directory'))

    # Run CLI function
    args = parser.parse_args()
    if not (args.hktabledir is None and args.hktableversion is None):
        loadhktable(
            hktabledir=args.hktabledir, hktableversion=args.hktableversion)
    main(args.ncfpath)
