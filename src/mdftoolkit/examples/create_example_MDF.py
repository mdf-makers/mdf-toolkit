#!/usr/bin/env python3
# -*- coding: utf-8 -*-  
"""

This is an example python program demonstrating how to generate an MDF
*and* to serve as a testbed for modifications to the MDF toolkit. Changes to
the MDF_spec code can be tested here before committing to be used by the
broader project.

This program simulates random data and then repacks it as if it were
observational data, creating an MDF file with 30 minute cadence data. This file
is to be updated whenever MDF definitions are updated so that it can be used
as a skeleton for anyone bootstrapping their own MDF processing code.

If you're just starting building your own MDF, you can copy this file to your
own site subdirectory and then modify the code to point to your data and do any
necessary processing.

"""

import sys, os

from time import sleep

import xarray   as xr # explicit dependency here but only really needed for 'df_from_ncfiles'
import numpy    as np
import pandas   as pd
import datetime as dt

import numpy.random as modnar # for generating fake data 

import netCDF4

# sys.path.insert(0,'../') # add ../ to imports, then grab the MDF spec 

# import inherited MDF Class that outlines the spec, needed functions, and where to get the data...
from mdftoolkit.MDF_toolkit import MDF_toolkit as MDF

from mdftoolkit.MDF_toolkit import (# some helper functions
    df_from_ncfiles,
    printline
)

def main():

    printline(); print("\nRunning the MDF toolkit over a fake dataset...\n"); printline()

    data_start_date = dt.date(2019,1,1)   
    data_end_date   = dt.date(2019,2,1) # it has been a while
 
    if ask_yn("Would you like to (re)create the fake data?"): create_fake_netcdf(data_start_date, data_end_date)

    # instantiate class that will analyze and output data in MDF format
    fake_MDF = MDF(supersite_name='example', verbose=False)
    global_atts = {
        "title"                    : "example MDF full of fake data",
        "Conventions"              : "MDF, CF (where possible)",
        "standard_name_vocabulary" : "",
        "contributor_name"         : "Susy Science",
        "contributor_email"        : "susy.science@institute.gov",
        "institution"              : "The Institute (ACRONYM)",
        "creator_name"             : "Patty Programmer",
        "creator_email"            : "patty.programmer@institute.edu",
        "project"                  : "Overarching project name",
        "summary"                  : "summarize it",
        "id"                       : "DOI: XXXXXXXX",
        "license"                  : "Creative Commons Attribution 4.0 License, CC 4.0", 
        "metadata_link"            : "http://www.doi-metatadat-link.org/mydoiiiii",
        "references"               : "paper name/DOI",
        "time_coverage_start"      : "{}".format(data_start_date),
        "time_coverage_end"        : "{}".format(data_end_date),
        "naming_authority"         : "___", 
        "standard_name_vocabulary" : "___", 
        "keywords"                 : "surface energy budget, arctic, polar",
    }
    fake_MDF.update_global_atts(global_atts)  

    my_data_df   = get_netcdf_data()               # get your data into a dataframe somehow
    print("\n\n"); print(my_data_df); print("\n\n")

    processed_df = my_process_function(my_data_df) # run the necessary data processing before writing MDF

    # map the variables from mydata_df to the modf_spec, this example is consistently updated
    # with all current modf variables i.e. you can copy this for your own uses to jumpstart your code
    var_map_dict = { 
                    'pa'            : 'new_var_avg',
                    'ua'            : 'my_var_name1',
                    'va'            : 'my_var_name2',
                    'ta'            : 'my_var_name3',
                    'tdp'           : 'my_var_name4',
                    'hus'           : 'my_var_name5', 
                    'hur'           : 'my_var_name6',
                    'thetawb'       : 'my_var_name7',
                    'rlu'           : 'my_var_name8',
                    'rld'           : 'my_var_name9',
                    'rsu'           : 'my_var_name10',
                    'rsd'           : 'my_var_name11',
                    'wthv'          : 'my_var_name12',
                    'wqv'           : 'my_var_name13',
                    'uw'            : 'my_var_name14',
                    'vw'            : 'my_var_name15',
                    'tke'           : 'my_var_name16',
                    'prsn'          : 'my_var_name17',
                    'z0m'           : 'my_var_name18',
                    'z0h'           : 'my_var_name19',
                    'psl'           : 'my_var_name20',
                    'ps'            : 'my_var_name21',
                    'uas'           : 'my_var_name22',
                    'vas'           : 'my_var_name23',
                    'zmla'          : 'my_var_name24',
                    'tas'           : 'my_var_name25',
                    'tdps'          : 'my_var_name26',
                    'huss'          : 'my_var_name27',
                    'pr'            : 'my_var_name28',
                    'clt'           : 'my_var_name29',
                    'cod'           : 'my_var_name30',
                    'prw'           : 'my_var_name31',
                    'clwvi'         : 'my_var_name32',
                    'clivi'         : 'my_var_name33',
                    'vias'          : 'my_var_name34',
                    'oz'            : 'my_var_name35',
                    'snd'           : 'my_var_name36',
                    'snw'           : 'my_var_name37',
                    'ts'            : 'my_var_name38',
                    'tsns'          : 'my_var_name39',
                    'tsn'           : 'my_var_name40',
                    'rhos'          : 'my_var_name41',
                    'tgs'           : 'my_var_name42',
                    'rsus'          : 'my_var_name43',
                    'rsds'          : 'my_var_name44',
                    'rlus'          : 'my_var_name45',
                    'rlds'          : 'my_var_name46',
                    'hfls_bulk'     : 'my_var_name47',
                    'hfls_ec'       : 'my_var_name48',
                    'hfss_bulk'     : 'my_var_name49',
                    'hfss_ec'       : 'my_var_name50',
                    'hfds'          : 'my_var_name51',
                    'hfdsn'         : 'my_var_name52',
                    'hfdsnb'        : 'my_var_name53',
                    'albs'          : 'my_var_name54',
                    'albsn'         : 'my_var_name55',
                    'tauu'          : 'my_var_name56',
                    'tauv'          : 'my_var_name57',
                    'tsl'           : 'my_var_name58',
                    'mrlsl'         : 'my_var_name59',
                    'to'            : 'my_var_name60',
                    'so'            : 'my_var_name61',
                    'uo'            : 'my_var_name62',
                    'vo'            : 'my_var_name63',
                    'wo'            : 'my_var_name64',
                    'tos'           : 'my_var_name65',
                    'mlotst'        : 'my_var_name66',
                    'hfsso'         : 'my_var_name67',
                    'hflso'         : 'my_var_name68',
                    'rsntds'        : 'my_var_name69',
                    'rlntds'        : 'my_var_name70',
                    'wfo'           : 'my_var_name71',
                    'tauuo'         : 'my_var_name72',
                    'tauvo'         : 'my_var_name73',
                    'sigwave'       : 'my_var_name74',
                    'siconc'        : 'my_var_name75',
                    'sithick'       : 'my_var_name76',
                    'sisnthick'     : 'my_var_name77',
                    'siu'           : 'my_var_name78',
                    'siv'           : 'my_var_name79',
                    'sisali'        : 'my_var_name80',
                    'sitemptop'     : 'my_var_name81',
                    'sitempsnic'    : 'my_var_name82',
                    'sitempbot'     : 'my_var_name83',
                    'sialb'         : 'my_var_name84',
                    'siflsensupbot' : 'my_var_name85',
                    'siflsenstop'   : 'my_var_name86',
                    'sifllatstop'   : 'my_var_name87',
                    'siflswdtop'    : 'my_var_name88',
                    'siflswutop'    : 'my_var_name89',
                    'sifllwdtop'    : 'my_var_name90',
                    'sifllwutop'    : 'my_var_name91',
                    'siflcondtop'   : 'my_var_name92',
                    'sipr'          : 'my_var_name93',
                    'ficonc'        : 'my_var_name94',
                    'fithick'       : 'my_var_name95',
                    'riconc'        : 'my_var_name96',
                    'rithick'       : 'my_var_name97',
                    'sitemp'        : 'my_var_name98',
    }

    # map the names to the modf format using
    print(processed_df)
    modf_df = MDF.map_vars_to_mdf(processed_df, var_map_dict, drop=True) # map and drop the variables we won't be using, get new df
    print("\n\n\n-------------------------------------------------------------")
    print(modf_df.columns)
    fake_MDF.add_data_timeseries(modf_df, cadence="time30")  # specify the cadence/frequency of data provided

    #fake_MDF.add_df_data(processed_df, var_map_dict, cadence="time01")  # could be combined into one call

    fake_MDF.add_data_scalar('height_tower', 23.3)   # add the height of the fake tower to the MDF, à la spec
    fake_MDF.add_data_scalar('height_radar', 5.0)    # and other scalars
    fake_MDF.add_data_scalar('depth_soilprobe', 1.0) # 
    fake_MDF.add_data_scalar('depth_iceprobe', 3.0)  # 
    fake_MDF.add_citations(var_names=modf_df.columns, citation='data product, DOI: XXXXXXXXX')

    # I haven't finished the sonde code yet, sorry, :( ripping it out for now
    # sonde_data = get_sonde_data()
    # fake_MDF.add_sonde_data(sonde_data)

    # then call loop that writes the fake_MDF vars to a NetCDF, must go last of course 
    fake_MDF.write_files(output_dir='./MDF_output/')

# end main()
# ------------------------------------------------------------------------------------------------------------

def my_process_function(df):
    # some processing to create necessary variables, your processing will *possibly* be more extensive
    df['new_var_avg'] = (df['my_var_name1']+df['my_var_name2'])/2.0
    return df 

# will find and open a list of sequential netcdf data files and concat them into one data frame
def get_netcdf_data():

    data_dir = './fake_data/'

    if not os.path.exists(data_dir):
        os.makedirs(data_dir)

    data_list = []
    for (root, subs, files) in os.walk(data_dir):
        for name in files:
            if name.endswith('.nc'):
                # selects some random ~3% of data so we run quickly and dont wait forever
                if modnar.randint(2) and modnar.randint(2) and modnar.randint(2):# and modnar.randint(2) and modnar.randint(2): #
                    data_list.append(root+name)

    
    df = df_from_ncfiles(data_list, verbose=True) # implemented as a helper function in the modf class
    return df

def create_fake_netcdf(start_date, end_date):

    data_dir = './fake_data/'

    var_list = ['var_thats_not_used','my_var_name1','my_var_name2','my_var_name3','my_var_name4','my_var_name5','my_var_name6','my_var_name7','my_var_name8','my_var_name9','my_var_name10','my_var_name11','my_var_name12','my_var_name13','my_var_name14','my_var_name15','my_var_name16','my_var_name17','my_var_name18','my_var_name19','my_var_name20','my_var_name21','my_var_name22','my_var_name23','my_var_name24','my_var_name25','my_var_name26','my_var_name27','my_var_name28','my_var_name29','my_var_name30','my_var_name31','my_var_name32','my_var_name33','my_var_name34','my_var_name35','my_var_name36','my_var_name37','my_var_name38','my_var_name39','my_var_name40','my_var_name41','my_var_name42','my_var_name43','my_var_name44','my_var_name45','my_var_name46','my_var_name47','my_var_name48','my_var_name49','my_var_name50','my_var_name51','my_var_name52','my_var_name53','my_var_name54','my_var_name55','my_var_name56','my_var_name57','my_var_name58','my_var_name59','my_var_name60','my_var_name61','my_var_name62','my_var_name63','my_var_name64','my_var_name65','my_var_name66','my_var_name67','my_var_name68','my_var_name69','my_var_name70','my_var_name71','my_var_name72','my_var_name73','my_var_name74','my_var_name75','my_var_name76','my_var_name77','my_var_name78','my_var_name79','my_var_name80','my_var_name81','my_var_name82','my_var_name83','my_var_name84','my_var_name85','my_var_name86','my_var_name87','my_var_name88','my_var_name89','my_var_name90','my_var_name91','my_var_name92','my_var_name93','my_var_name94','my_var_name95','my_var_name96','my_var_name97','my_var_name98']

    fill_val = netCDF4.default_fillvals['f8']

    var_att_dict = {}
    for var in var_list :
        var_att_dict[var] = {'units' : 'random'}

    global_atts = {
        "title"                    : "THIS FILE CONTAINS RANDOMLY GENERATED FAKE DATA TO TEST MDF CODE",
        "time_coverage_start"      : "{}".format(start_date),
        "keywords"                 : "surface energy budget, arctic, polar",
    }
    day_delta = pd.to_timedelta(86399999999,unit='us') # we want to go up to but not including 00:00
    dates     = pd.date_range(start_date, end_date, freq='D')
    for today in dates:
        if today.day == 1 or today.day == 15: print("... creating data for day {}".format(today))
        tomorrow = today+day_delta
        global_atts.update({"time_coverage_end": "{}".format(today+dt.timedelta(1))})

        fake_data_index = pd.date_range(today, tomorrow, freq='30T') # create fake observations every 10 minutes
        fdi = fake_data_index                                        # some shorthand names
        bot = np.datetime64(start_date)                              # create array 'since beginning of time', to index netcdf files

        file_str = '{}/fake_date_{}.nc'.format(data_dir,today.strftime('%Y%m%d'))
        ncdf     = netCDF4.Dataset(file_str, 'w')

        for att_name, att_val in global_atts.items(): # write global attributes 
            ncdf.setncattr(att_name, att_val)

        # unlimited dimension to show that time is split over multiple files (makes dealing with data easier)
        ncdf.createDimension('time', None)

        time_atts   = {'units'     : 'seconds since {}'.format(bot),
                       'delta_t'   : '0000-00-00 30:00:00',
                       'long_name' : 'seconds since the first day of observations',
                       'calendar'  : 'standard',}


        # create the arrays that are integer intervals expected by netcdf 
        delta_ints = np.floor((fdi - bot).total_seconds()) # seconds
        time_inds  = pd.Int64Index(delta_ints)

        nvals = len(time_inds)

        # set the time dimension and variable attributes to what's defined above
        t    = ncdf.createVariable('time', 'int32','time') # seconds since
        t[:] = time_inds.values
        for att_name, att_val in time_atts.items(): ncdf['time'].setncattr(att_name,att_val)

        # set variable attributes from above and generate data
        for var_name, var_atts in var_att_dict.items():

            var = ncdf.createVariable(var_name, np.float32, 'time')

            for att_name, att_desc in var_atts.items(): ncdf[var_name].setncattr(att_name, att_desc)

            random_random_function, random_random_function_args = get_random_random_function_function()
            random_random_function_args['size'] = nvals
            rand_data = pd.DataFrame(random_random_function(**random_random_function_args))

            max_val = np.nanmax(rand_data.values) # masked array max/min/etc
            min_val = np.nanmin(rand_data.values)
            avg_val = np.nanmean(rand_data.values)

            ncdf[var_name].setncattr('max_val', max_val)
            ncdf[var_name].setncattr('min_val', min_val)
            ncdf[var_name].setncattr('avg_val', avg_val)

            n_to_make_missing = modnar.randint(nvals)
            kill_these_vals = modnar.randint(nvals, size=n_to_make_missing)

            rand_data.loc[kill_these_vals] = np.nan
            rand_data.fillna(fill_val, inplace=True)

            var[:] = rand_data.values

            # write atts to the var now
            for att_name, att_desc in var_atts.items(): ncdf[var_name].setncattr(att_name, att_desc)
            ncdf[var_name].setncattr('missing_value', fill_val)

            # add a percent_missing attribute to give a first look at "data quality"
            perc_miss = perc_missing(rand_data.values)
            ncdf[var_name].setncattr('percent_missing', perc_miss)

        ncdf.close() # close and write files for today

def get_random_random_function_function(): # says what it does, does what it says

    rg = modnar.Generator(modnar.PCG64())

    # 9 random number generators
    rand_funcs = (rg.beta, rg.chisquare, rg.binomial, rg.f,
                  rg.gamma, rg.laplace, rg.pareto, rg.uniform, rg.weibull)

    n_modnar_funcs = 9

    low  = np.abs(random_reasonable_float())
    high = np.abs(random_reasonable_float())
    if low > high: new_high=low; low=high; high=new_high;
        
    func_args  = ({'a': np.abs(random_float()) , 'b': np.abs(random_float())},
                  {'df':modnar.randint(1,10),},
                  {'n':modnar.randint(1,modnar.randint(2,100)), 'p':modnar.random_sample()},
                  {'dfnum':modnar.randint(1,10), 'dfden':modnar.randint(1,10),},
                  {'shape': np.abs(200 * modnar.random_sample() - 100),'scale': np.abs(200 * modnar.random_sample() - 100)},
                  {'loc': random_reasonable_float(), 'scale': np.abs(random_reasonable_float())},
                  {'a': np.abs(random_reasonable_float())},
                  {'low': low, 'high': high},
                  {'a': np.abs(random_reasonable_float())},
                  )

    func_number = modnar.randint(0, n_modnar_funcs)
    return (rand_funcs[func_number], func_args[func_number])

def random_float():
    big_number = 1000000000000000
    return 2*big_number * modnar.random_sample() - big_number

def random_reasonable_float():
    big_number = 10000
    return 2*big_number * modnar.random_sample() - big_number

def perc_missing(series):
    if series.size == 0: return 100.0
    return np.round((np.count_nonzero(np.isnan(series))/float(series.size))*100.0, decimals=4)

def ask_yn(question):
    valid  = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    while True:
        print(question)
        choice = input(" ---> ").lower()
        if choice in valid:
            print("")
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' (or 'y' or 'n').\n\n")

def bytes_to_hr(byte_size): # humanreadable string
    power = 2**10
    n=0
    while byte_size > power: byte_size/=power; n+=1
    power_labels = {0:"", 1:"kilo", 2:"mega", 3:"giga", 4:"tera"}
    return "{} {}bytes".format(round(byte_size,4),power_labels[n])

# actually run the program (allows functions to be defined at bottom)
if __name__ == '__main__':
    main()
