#!/usr/bin/env python3
# ------------------------------------------------------------------------------
#
# This is the SPLASH implementation of the MODF toolkit spec...
#
# Developed by:
# michael.r.gallagher@noaa.gov
#
# https://gitlab.com/modf-makers/modf-toolkit/
#
import sys, os, argparse, time

import numpy  as np
import pandas as pd
import xarray as xr

from datetime  import datetime, timedelta
from netCDF4   import Dataset, MFDataset, num2date

try:
    from debug_functions import drop_me as dm
except ModuleNotFoundError:
    dm = None

nthreads = 16 # we're going to do some threaded reads and writes to save time
 
# import MODF Class that outlines the spec, functions, and where to get the data...
from modftoolkit.MODF_toolkit import MODF_toolkit 

from modftoolkit.MODF_toolkit import(     #  along with some helper functions
    get_code_version,
    get_person_to_blame,
    printline,
    regrid_vertical
)

from multiprocessing import Process as P # threaded reads of files
from multiprocessing import Queue   as Q
from multiprocessing import Pool 

# this should be removed/temporary, there are some errant xarray messages
import warnings
warnings.simplefilter("ignore", FutureWarning) # xarray messages

# ############################################################################################################
# called by __main__
def create_splash_modf(start_time=None, end_time=None, data_dir=None, pickle_dir=None, verbose=True):

    print(f"{sys.version}")

    printline(end="\n\n")
    print(f"  Begin processing SPLASH data for output to MODF specced format...")
    print(f"  ...")
    print(f"  ... currently using MODF toolkit version {get_code_version()}")
    print(f"  ... please contact {get_person_to_blame()} if something doesn't run right")
    print(f"  ...")

    # instantiate class that will analyze and output data in MODF format, multi_site means more than one geographic observation point
    MODF = MODF_toolkit(supersite_name='SPLASH', verbose=verbose, nthreads=nthreads, multi_site=True)

    # add some metadata about the data just added
    # MODF.add_data_scalar('height_tower', 30.0)            # add the height of the fake tower to the MODF, à la spec
    # MODF.add_data_scalar('depth_iceprobe', 0.10)          # ?? flux plate depth, not really correct
    # MODF.add_data_scalar('orog', 0.0)                     # 

    day_delta  = pd.to_timedelta(86399999999,unit='us') # we want to go up to but not including 00:00
    end_time = end_time+day_delta

    global_atts = {
        "title"                    : "SPLASH campaign data in MODF specced format",
        "Conventions"              : "MODF, CF (where possible)",
        "standard_name_vocabulary" : "",
        "contributor_name"         : "Gijs de Boer",
        "contributor_email"        : "gijs.deboer@noaa.gov",
        "institution"              : "CIRES/NOAA/ESRL",
        "creator_name"             : "Michael Gallagher",
        "creator_email"            : "michael.r.gallagher@noaa.gov",
        "project"                  : "SPLASH field campaign",
        "summary"                  : "", 
        "id"                       : "THIS IS A BETA PRODUCT NOT YET SUITABLE FOR RESEARCH PURPOSES",
        "license"                  : "Creative Commons Attribution 4.0 License, CC 4.0", 
        "metadata_link"            : "",
        "references"               : "",
        "time_coverage_start"      : "{}".format(start_time),
        "time_coverage_end"        : "{}".format(end_time+timedelta(1)),
        "naming_authority"         : "", 
        "keywords"                 : "surface energy budget, arctic, polar",
    }
    MODF.update_global_atts(global_atts)  

    # get the data and use the metadata here to fill in some global attributes
    stations = ['asfs30', 'asfs50']
    station_data = {}
    print("  ... getting asfs data")
    for station in stations: 
        station_data[station], data_version = get_asfs_data(station, start_time, end_time,
                                                            1, data_type='slow', nthreads=nthreads,
                                                            verbose=False, pickle_dir=pickle_dir)
        # if the full file was pickled, then we have to narrow the scope
        station_data[station] = station_data[station][start_time:end_time] 
        station_data[station]['fp_Avg'] = (station_data[station]['fp_A_Wm2_Avg'] \
                                           + station_data[station]['fp_A_Wm2_Avg'])/2 # average of both?
        # unit conversions
        station_data[station]['vaisala_T_Avg'] = station_data[station]['vaisala_T_Avg'] + 273.15
        station_data[station]['albedo'] = (station_data[station]['sr30_swd_Irr_Avg']/station_data[station]['sr30_swu_Irr_Avg'])
        #station_data[station]["skin_temp_surface"]     = station_data[station]["skin_temp_surface"]     + 273.15
        #station_data[station]["temp_acoustic_mean_2m"] = station_data[station]["temp_acoustic_mean_2m"] + 273.15

        MODF.add_subsite(station)
        MODF.add_data_scalar('lat',
                             station_data[station]['gps_lat_deg_Avg'].mean()\
                             +station_data[station]['gps_lat_min_Avg'].mean()/60,
                             station)
        MODF.add_data_scalar('lon',
                             station_data[station]['gps_lon_deg_Avg'].mean()\
                             +station_data[station]['gps_lon_min_Avg'].mean()/60,
                             station)
        
    asfs_var_map = { # map the station variables to the modf_spec
        'albs'   : 'albedo', 
        'rsu'    : 'sr30_swu_Irr_Avg', 
        'rsd'    : 'sr30_swd_Irr_Avg', 
        'ts'     : 'apogee_targ_T_Avg',
        'hur'    : 'vaisala_RH_Avg',
        'ta'     : 'vaisala_T_Avg',
        'tdp'    : 'vaisala_Td_Avg', 
        'ps'     : 'vaisala_P_Avg', 
        'rlu'    : 'ir20_lwu_Wm2_Avg',  
        'rld'    : 'ir20_lwd_Wm2_Avg',
        'hfdsnb' : 'fp_Avg', # average of both?
    }# need to be added later
    # 'ua'        : 'wspd_u_mean_2m',
    # 'va'        : 'wspd_v_mean_2m',
    # 'tas'       : 'temp_acoustic_mean_2m',
    
    MODF.add_citations(var_names=asfs_var_map.keys(), citation='SPLASH group ASFS data product, DOI: XXXXXXXXX')

    sd = {}
    for station in stations:
        sd[station] = MODF.map_vars_to_modf(station_data[station], asfs_var_map, True, station) 
        # map and drop variables we won't be using

    print(" ... getting turb data")
    td = {}
    for station in stations:
        td[station], data_version = get_asfs_data(station, start_time, end_time, 1, data_dir,  
                                                  'turb', nthreads=nthreads, verbose=False, pickle_dir=pickle_dir)
        td[station] = td[station][start_time:end_time]

    lev2_10min_var_map = { # now add some 10 minute cadence variables
        'wqv'       : 'Hl', 
        'wthv'      : 'Hs', 
        'hfls_bulk' : 'bulk_Hl',
        'hfss_bulk' : 'bulk_Hs',
    }

    for station in stations:
        printline(start='\n\n')
        td[station] = MODF.map_vars_to_modf(td[station], lev2_10min_var_map, drop=True) # map and drop the variables we won't be using

        # pass off the data to be written and cite the dataset
        MODF.add_data_timeseries(sd[station], subsite_name=station, cadence="time01") #specify the cadence? necessary? or can add_data infer this
        MODF.add_data_timeseries(td[station], subsite_name=station, cadence="time10") #specify the cadence? necessary? or can add_data infer this

    # ##############################################################################################################
    # roaring judy files
    rjy_df = get_rjy_data()
    rjy_df = rjy_df[start_time:end_time]

    rjy_df['wspd_u']  = -1*rjy_df['ScalarWindSpeed']*np.sin(rjy_df['WindDirection'])
    rjy_df['wspd_v']  = -1*rjy_df['ScalarWindSpeed']*np.cos(rjy_df['WindDirection'])
    rjy_var_map = { # map the station variables to the modf_spec
        'hur'    : 'RelativeHumidity',
        'ta'     : 'Temperature',
        'ps'     : 'Pressure',
        'ua'     : 'wspd_u',
        'va'     : 'wspd_v',
    }

    rjy_df = MODF.map_vars_to_modf(rjy_df, rjy_var_map, drop=True) # map and drop the variables we won't be using
    MODF.add_subsite         ('roaring') # these could be combined in the toolkit, I suppose
    MODF.add_data_scalar     ('lat', 38.7055506, 'roaring')
    MODF.add_data_scalar     ('lon', -106.8542253,'roaring')
    MODF.add_data_timeseries (rjy_df, subsite_name='roaring', cadence="time02") 

    # ##############################################################################################################
    #  files
    gothic_df = get_guc_data(start_time, end_time, True, pickle_dir)
    gothic_df = gothic_df[start_time:end_time]
 
    gothic_df['wspd_u']  = -1*gothic_df['wspd_vec_mean']*np.sin(gothic_df['wdir_vec_mean'])
    gothic_df['wspd_v']  = -1*gothic_df['wspd_vec_mean']*np.cos(gothic_df['wdir_vec_mean'])
    gothic_var_map = { # map the station variables to the modf_spec
        'hur'    : 'rh_mean',
        'ta'     : 'temp_mean',
        'ps'     : 'atmos_pressure',
        'ua'     : 'wspd_u',
        'va'     : 'wspd_v',
    }

    gothic_df = MODF.map_vars_to_modf(gothic_df, gothic_var_map, drop=True) # map and drop the variables we won't be using
    MODF.add_subsite('gothic') # these could be combined in the toolkit, I suppose
    MODF.add_data_scalar('lat', 38.9549646, 'gothic')
    MODF.add_data_scalar('lon', -106.9893014, 'gothic')
    MODF.add_data_timeseries(gothic_df, subsite_name='gothic', cadence="time02") 

    # ##############################################################################################################
    MODF.write_files(output_dir='./')

    # then call function that writes the MODF vars to a NetCDF, must go last of course 
    return MODF



def get_asfs_data(station, start_day, end_day, level,
                  data_dir='/PSL/Observations/Campaigns/SPLASH/', data_type='slow',
                  verbose=False, nthreads=1, pickle_dir=None):

    """ Get a dataset from the SPLASH flux project. 

    Function assumes the standard folder structure as in the NOAA archive. 
    Supports parallelism without using heavy libraries, like dask. 

    Required params
    ---------------
    station     : str station name, used to determine directory and file name, 'asfs30', etc 'tower'
    start_day   : datetime object, used to determine file names
    end_day     : datetime object, used to determine when to stop
    level       : 1, 2, ... which dataset 

    Optional params
    ---------------
    data_dir    : the head where the files can be found
    data_type   : 'fast' 'slow', 'turb','met'data, etc
    nthreads    : how many cpu threads would you like to use 
    verbose     : would you like to see more print statements?

    pickle_dir : if provided, we search for a pandas 'pickle' binary
                 file containing the pre-packed pandas objects. if we
                 don't find it, it is written after ingest. saves *a lot*
                 of time when stored on ramdisk for bigger files.

    Returns
    -------
    tuple (df pandas.DataFrame, str code_version)

             df contains all data between requested days 
             code version is read from file netcdf attributes

    """

    station_list = ['asfs30','asfs50']
    station_dict = {'asfs30':'pond', 'asfs50':'picnic'}

    if not any(station == name for name in station_list):
        print("\n\nYou asked for a station name that doesn't exist...")
        print("... can't help you here\n\n"); raise IOError

    pickled_filename = f'{station}_{level}_{data_type}_df' # code_version is tacked on to this when run
    df = pd.DataFrame() # dataframe and version we return from this function
    code_version = "?"
    was_pickled = False
    if pickle_dir:
        print(f"\n!!! searching for pickle file containing {pickled_filename} and loading it, takes time...\n")
        for root, dirs, files in os.walk(pickle_dir): #pretty stupid way to find the file
            for filename in files:
                if pickled_filename in filename:
                    filename = pickle_dir+filename
                    df = pd.read_pickle('.//'+filename)
                    name_words = filename.rpartition('_')[-1].rpartition('.')
                    code_version = f"{name_words[0]}.{name_words[1]}"
                    was_pickled = True
                    print(f" ... found and loaded pickle {filename} \n\n")
                    break

        if not was_pickled: print("... didn't find a pickle, we'll write one !!!\n\n")

    if not was_pickled: 
        df_list = [] # data frames get appended here in loop and then concatted by function after
        day_series = pd.date_range(start_day, end_day) 

        q_list = []; p_list = []; day_list = []
        for i_day, today in enumerate(day_series): # loop over days in processing range and get list of files

            if i_day %nthreads == 0:
                if nthreads > 1:
                    print("  ... getting data for day {} (and {} days after in parallel)".format(today,nthreads))
                else:
                    print("  ... getting data for day {}".format(today))

            date_str = today.strftime('%Y%m%d.%H%M%S')
            if level == 1: level_str = 'ingest'
            if level == 2: level_str = 'product'
            if level == 3: level_str = 'archive'

            if data_type == 'turb': date_str = date_str+'.10min'
            subdir   = f'/{level}_level_{level_str}/'
            file_str = f'/{data_type}sled.level{level}.{station}-{station_dict[station]}.{date_str}.nc'
            
            if level == 2:
                    file_str = f'/mos{data_type}.{station}.level{level}v2.1min.{date_str}.nc'
                    subdir = subdir+'/'

            files_dir = data_dir+station+subdir
            curr_file = files_dir+file_str

            q_today = Q()
            P(target=get_datafile, args=(curr_file, q_today),).start()
            q_list.append(q_today)
            day_list.append(today)
            if (i_day+1) % nthreads == 0 or today == day_series[-1]:
                for iq, qq in enumerate(q_list):
                    df_today = qq.get()
                    cv = qq.get()
                    if cv!=None: code_version = cv # assume all files have same code version, save only one
                    if not df_today.empty: 
                        df_list.append(df_today.copy())
                q_list = []
                day_list = []

        if verbose: print("... concatting, takes some time...")
        try    : df = pd.concat(df_list)
        except : pd.DataFrame()

        try: 
            df.index = df.index.droplevel("freq")
            df       = df[~df.index.duplicated(keep='first')]
        except: pass # this only applices to 'seb'

        time_dates = df.index
        df['time'] = time_dates # duplicates index... but it can be convenient

        if verbose:
            print('\n ... data sample :')
            print('================')
            print(df)
            print('\n')
            print(df.info())
            print('================\n\n') 

        if pickle_dir:
            print("\n\n!!! You requested to pickle the dataframe for speed !!!")
            print(f"... right now we're writing it to this directory {pickled_filename}")
            print("... copy this manually to a ramdisk somewhere for bonus speed points")
            print("... must be symlinked here to be seen by this routine")
            print("...\n... this takes a minute, patience\n\n")
            df.to_pickle(f"{pickle_dir}/{pickled_filename}_{code_version[0:3]}.pkl")

    return df, code_version[0:3]


def get_mosaic_sonde_data(start_time, end_time, sonde_dir, verbose=False):

    import glob
    sonde_file_list = glob.glob(sonde_dir+"/*.txt")
    tot_files       = len(sonde_file_list)

    print(f"  ... FOUND {tot_files} sonde files ... opening...")

    col_names = ["HeightGnd","GpsHeightGnd","P","Temp","RH","Dir","Speed"]

    sonde_df_list = []
    for ifile, sfile in enumerate(sonde_file_list):
        with open(sfile, 'rb') as sf: 
            for il, line in enumerate(sf): # get appropriate lines and encode them as ascii strs only
                if il==3: launch_time = line.decode("ascii")
                if il==4: launch_date = line.decode("ascii"); break
        
        dt_str = "".join(launch_time.split())+"-"+"".join(launch_date.split()) # fancy/simple way to strip weird chars
        launch_dt = datetime.strptime(dt_str, "%d/%m/%y-%X") 

        if launch_dt < start_time or launch_dt > end_time:
            tot_files -= 1
            continue
        try:
            sdf = pd.read_csv(sfile, sep='\s+', header=[19,20], encoding = "ISO-8859-1")

        except Exception as e:
        #except UnicodeDecodeError as e:
        #except pd.errors.ParserError as e:
            if verbose:
                print(f"  ... unable to open sonde {sfile}, exception:")
                print(f"  ... {e}")
            continue

        sdf.columns = sdf.columns.get_level_values(0) # remove units from multi-index
        # vaisala time resolution is 1 second... 
        # the mean radiosonde ascent velocity is ~5 m/s.
        dti = pd.date_range(launch_dt, periods=len(sdf), freq="s") # set index to every second
        sdf = sdf.set_index(dti)
        sdf = sdf.apply(pd.to_numeric, errors='coerce') # there are '////' values for nan's in the file 0_o
        sonde_df_list.append(sdf)

    print(f"  ... USED a total of {tot_files}, returning! ")

    return sonde_df_list

def get_rjy_data():
    header=['DataloggerID','year','julianday','hoursminutes','Pressure (mb)','Temperature (C)','RelativeHumidity (%)','ScalarWindSpeed (m/s)','VectorWindSpeed (m/s)','WindDirection (degrees)','WindDirectionStandardDeviation (degrees)','BatteryVoltage (volts)','MaximumWindSpeed (m/s)']

    header=['DataloggerID','year','julianday','hoursminutes','Pressure','Temperature','RelativeHumidity','ScalarWindSpeed','VectorWindSpeed','WindDirection','WindDirectionStandardDeviation','BatteryVoltage','MaximumWindSpeed']

    na_vals = ['nan','NaN','NAN','NA','\"INF\"','\"-INF\"','\"NAN\"','\"NA','\"NAN','inf','-inf''\"inf\"','\"-inf\"','']
    flist   = get_file_list(data_dir='/home/mgallagher/data/SPLASH/rjy/')
    print(f"... getting {len(flist)} files for Roaring Judy site")

    rjy_dp  = lambda x: datetime.strptime(x, '%Y %j %H%M')

    df_list = [pd.read_csv(f, sep=',', names=header, na_values=na_vals, parse_dates={'time':['year','julianday','hoursminutes']}, date_parser=rjy_dp) for f in flist]
            
    complete_df = pd.concat(df_list).set_index('time').sort_index().drop_duplicates()
    complete_df = complete_df[~complete_df.index.duplicated(keep='first')]
    print(f"... got the RJY files, with data ranging from {complete_df.index[0]} ------> {complete_df.index[-1]}")
    return complete_df


def get_file_list(data_dir, prefix_str=None):
    flist = []
    for root, dirs, files in os.walk(data_dir):
        if prefix_str is None: flist+=[os.path.join(root, name) for name in files]
        else: flist+=[os.path.join(root, name) for name in files if len(name.split(prefix_str)) > 1]

    return flist

def get_brush_data():
    do_nothing = True # wtf are the headers to the ARL files... etc etc

def get_guc_data(start_day, end_day, verbose=False, pickle_dir=None):
    data_dir   = '/home/mgallagher/data/SPLASH/guc/' # arm netcdf files
    flist      = get_file_list(data_dir, 'gucmetM1')
    day_series = pd.date_range(start_day, end_day) 

    pickled_filename = f'guc_{start_day}_{end_day}_df' # code_version is tacked on to this when run
    was_pickled = False

    if pickle_dir:
        print(f"\n!!! searching for pickle file containing {pickled_filename} and loading it, takes time...\n")
        for root, dirs, files in os.walk(pickle_dir): # pretty stupid way to find the file
            for filename in files:
                if pickled_filename in filename:
                    filename = pickle_dir+filename
                    df = pd.read_pickle('.//'+filename)
                    name_words = filename.rpartition('_')[-1].rpartition('.')
                    code_version = f"{name_words[0]}.{name_words[1]}"
                    was_pickled = True
                    print(f" ... found and loaded pickle {filename} \n\n")
                    break

        if not was_pickled: print("... didn't find a pickle, we'll write one !!!\n\n")

    if not was_pickled: 

        df_list = []
        q_list = []; p_list = []; day_list = []
        for i_file, file_str in enumerate(flist): # loop over days in processing range and get list of files

            if i_file %nthreads == 0:
                if nthreads > 1: print("  ... getting GUC data file {} (and {} days after in parallel)".format(file_str,nthreads))
                else: print("  ... getting GUC data from file {}".format(file_str))

            curr_file = file_str

            q_today = Q()
            P(target=get_datafile, args=(curr_file, q_today, True, True),).start()
            q_list.append(q_today)
            day_list.append(file_str)
            if (i_file+1) % nthreads == 0 or file_str == flist[-1]:
                for iq, qq in enumerate(q_list):
                    df_today = qq.get()
                    cv = qq.get()
                    if cv!=None: code_version = cv # assume all files have same code version, save only one
                    if not df_today.empty: 
                        df_list.append(df_today.copy())
                q_list = []
                day_list = []

        if verbose: print("\n ... concatting, takes some time...")
        try    : df = pd.concat(df_list).sort_index().drop_duplicates()
        except : pd.DataFrame()

        if verbose:
            print('\n ... data sample :')
            print('================')
            print(df)
            print('\n')
            print(df.info())
            print('================\n\n') 


        if pickle_dir:
            print("\n\n!!! You requested to pickle the dataframe for speed !!!")
            print(f"... right now we're writing it to this directory {pickled_filename}")
            print("... copy this manually to a ramdisk somewhere for bonus speed points")
            print("... must be symlinked here to be seen by this routine")
            print("...\n... this takes a minute, patience\n\n")
            df.to_pickle(f"{pickle_dir}/{pickled_filename}_{code_version[0:3]}.pkl")

    return df

# simple wrapper to use xarray to convert CF time conventions
# automagically but to return a df because they are nice for timeseries
def get_datafile(curr_file, q=None, want_df=True, drop_bounds=False):
    if os.path.isfile(curr_file): xarr_ds = xr.open_dataset(curr_file)
    else:
        print(f"!!! requested file doesn't exist : {curr_file}");
        xarr_ds = xr.Dataset(coords={'time':[]}) # empty xarray

    try:
        if want_df:
            if drop_bounds:
                xarr_ds = xarr_ds.drop_dims('bound')
            data_frame   = xarr_ds.to_dataframe()
            code_version = 'nla'
            q.put(data_frame)
            q.put(code_version)
        else:
            q.put(xarr_ds)

    except Exception as e:
        if want_df: return data_frame # can be implicit but doesn't matter, really
        else: 
            print("returning ds")
            return xarr_ds

# a function that resamples and then averages a dataframe column with a requirement
# on the percentage of points that are non-nan in each period resampled
def resample_and_average(data_frame, var_name, resample_period='10min', percent_required=50):

    # calculate the data points per minute, to then 
    orig_period = np.diff(data_frame.index.values).min().astype(int)/6e10 # native observation period 
    new_period  = float(resample_period.strip('min')) # desired period
    num_points  = np.ceil(np.floor(new_period/orig_period)*(percent_required/100.0))

    def my_mean(series, min_count):
        if len(series) - series.isnull().sum() >=  min_count: return series.mean()
        else: return np.nan

    rsamped_series = data_frame[var_name].resample(resample_period).agg(my_mean, min_count=num_points) 

    return rsamped_series

# ################################################################################################
# actually run the program (allows functions to be defined at bottom) and...
# ... through a (c)python quirk makes things run faster
# 
# https://stackoverflow.com/questions/11241523/why-does-python-code-run-faster-in-a-function
if __name__ == '__main__':

    # first, define some program options/flags, not required but makes command line interaction easier
    # ################################################################################################
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start_time',  metavar='str',   help='beginning of processing period, Ymd syntax')
    parser.add_argument('-e', '--end_time',    metavar='str',   help='end  of processing period, Ymd syntax')
    parser.add_argument('-p', '--path',        metavar='str',   help='full data path, including trailing slash') # make portable
    parser.add_argument('-pd', '--pickle_dir', metavar='str',   help='full data path, including trailing slash') # make portable
    parser.add_argument('-v', '--verbose',    action ='count', help='print verbose log messages')
 
    args = parser.parse_args()

    # create a verbose print function 
    if args.verbose: verbose = True
    else: verbose = False
    v_print      = print if args.verbose else lambda *a, **k: None
    verboseprint = v_print

    # paths
    global level1_dir, level2_dir, turb_dir, raise_day 

    if args.path: data_dir = args.path
    elif data_dir==None: data_dir = '/Projects/SPLASH/'

    if args.pickle_dir: pickle_dir=args.pickle_dir
    elif pickle_dir==None: pickle_dir=False

    level2_dir = data_dir+'/tower/2_level_product/'        # where does level2 data go
    turb_dir   = data_dir+'/tower/2_level_product/'        # where does level2 data go

    #global start_time, end_time # time range we'll process data over
    if args.start_time:
        start_time = datetime.strptime(args.start_time, '%Y%m%d')
    elif start_time==None:
        # make the data processing at beginning of mosaic
        start_time = datetime(2019,10,15)
        start_time = start_time.replace(hour=0, minute=0, second=0, microsecond=0, day=start_time.day)

    if args.end_time:
        end_time = datetime.strptime(args.end_time, '%Y%m%d')
    elif end_time==None:
        end_time = datetime.today() # any datetime object can provide current time
        end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0, day=start_time.day)

    printline(end='\n\n')
    print('  ... the first day we  process data is:     %s' % str(start_time))
    print('  ... the last day we will process data is:  %s' % str(end_time))
    printline(start="\n\n", end="\n")
    time.sleep(3)

    create_splash_modf(start_time, end_time, data_dir, pickle_dir, verbose)

