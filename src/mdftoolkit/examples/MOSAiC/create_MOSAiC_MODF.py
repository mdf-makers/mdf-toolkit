#!/usr/bin/env python3
# ------------------------------------------------------------------------------
#
# This is the MOSAiC implementation of the MODF toolkit specification that takes
# in a range of observations from the MOSAiC year and outputs a correctly
# formatted YOPP standardized "MODF" (merged observatory data file) for the
# MOSAiC data set... !
#
# If you're looking at this file for guidance so you can ingest your own data,
# please try to follow the same format. Breaking your data ingest routines into
# their own functions. This way, the "main" program serves largely as
# documentation of the major steps taken to create the MODF and the source of
# variables in the resulting MODF file. Ideally source code can become
# documentation!
#
# This is still a work in progress, a beta-data-product. I'm,
# (michael.r.gallagher@noaa.gov) currently working on expanding and formalizing
# the code so that it is in shape to be utilized by other folks. Please give me
# any feedback you have, either here or in the gitlab repository:
#
# https://gitlab.com/modf-makers/modf-toolkit/
#
import sys, os, argparse, time

import numpy  as np
import pandas as pd
import xarray as xr

from datetime  import datetime, timedelta
from netCDF4   import Dataset, MFDataset, num2date

nthreads = 16 # we're going to do some threaded reads and writes to save time
 
try:
    from debug_functions import drop_me as dm
    #dm(locals().update(globals()),0)

except ModuleNotFoundError:
    dm = None

# import MODF Class that outlines the spec, functions, and where to get the data...
from modftoolkit.MODF_toolkit import MODF_toolkit 

from modftoolkit.MODF_toolkit import (     #  along with some helper functions
    get_code_version,
    get_person_to_blame,
    printline,
    regrid_vertical
)

from multiprocessing import Process as P # threaded reads of files
from multiprocessing import Queue   as Q
from multiprocessing import Pool 

# this should be removed/temporary, there are some errant xarray messages
import warnings
warnings.simplefilter("ignore", FutureWarning) # xarray messages

# ############################################################################################################
# called by __main__
def create_mosaic_modf(start_time=None, end_time=None, data_dir=None, pickle_dir=None, verbose=True):

    print(f"{sys.version}")

    printline(end="\n\n")
    print(f"  Begin processing MOSAiC data for output to MODF specced format...")
    print(f"  ...")
    print(f"  ... currently using MODF toolkit version {get_code_version()}")
    print(f"  ... please contact {get_person_to_blame()} if something doesn't run right")
    print(f"  ...")

    # instantiate class that will analyze and output data in MODF format
    MODF = MODF_toolkit(supersite_name='MOSAiC', verbose=verbose, nthreads=nthreads)

    # add some metadata about the data just added
    MODF.add_data_scalar('height_tower', 30.0)            # add the height of the fake tower to the MODF, à la spec
    MODF.add_data_scalar('depth_iceprobe', 0.10)          # ?? flux plate depth, not really correct
    MODF.add_data_scalar('orog', 0.0)                     # 

    day_delta  = pd.to_timedelta(86399999999,unit='us') # we want to go up to but not including 00:00
    end_time = end_time+day_delta

    global_atts = {
        "title"                    : "MOSAiC drifting campaign data in MODF specced format",
        "Conventions"              : "MODF, CF (where possible)",
        "standard_name_vocabulary" : "",
        "contributor_name"         : "Matthew Shupe",
        "contributor_email"        : "matthew.shupe@noaa.gov",
        "institution"              : "CIRES/NOAA/ESRL",
        "creator_name"             : "Michael Gallagher",
        "creator_email"            : "michael.r.gallagher@noaa.gov",
        "project"                  : "MOSAiC field campaign",
        "summary"                  : "", 
        "id"                       : "THIS IS A BETA PRODUCT NOT YET SUITABLE FOR RESEARCH PURPOSES",
        "license"                  : "Creative Commons Attribution 4.0 License, CC 4.0", 
        "metadata_link"            : "",
        "references"               : "",
        "time_coverage_start"      : "{}".format(start_time),
        "time_coverage_end"        : "{}".format(end_time+timedelta(1)),
        "naming_authority"         : "", 
        "keywords"                 : "surface energy budget, arctic, polar",
    }
    MODF.update_global_atts(global_atts)  

    # get the data and use the metadata here to fill in some global attributes
    print(" ... getting met data")
    lev2_df, data_version = get_flux_data('tower', start_time, end_time, 2, data_dir,  
                                          'met', nthreads=nthreads, verbose=False, pickle_dir=pickle_dir)
    
    lev2_df       = lev2_df[start_time:end_time] # if the full file was pickled, then we have to narrow the scope

    lev2_var_map = { # map the variables from lev2_df to the modf_spec
                     'lat'       : 'lat_tower',
                     'lon'       : 'lon_tower',
                     'ps'        : 'atmos_pressure_2m',
                     'ua'        : 'wspd_u_mean_2m',
                     'va'        : 'wspd_v_mean_2m',
                     'ta'        : 'temp_2m',
                     'ts'        : 'skin_temp_surface',
                     'tdp'       : 'dew_point_2m',
                     'hur'       : 'rh_2m',
                     'hus'       : 'mixing_ratio_2m',
                     'ps'        : 'atmos_pressure_2m',
                     'uas'       : 'wspd_u_mean_10m',
                     'vas'       : 'wspd_v_mean_10m',
                     'tas'       : 'temp_acoustic_mean_2m',
                     'rlu'       : 'up_long_hemisp',  
                     'rld'       : 'down_long_hemisp',
                     'rsu'       : 'up_short_hemisp',  
                     'rsd'       : 'down_short_hemisp',
                     'tdps'      : 'dew_point_2m',
                     'tsns'      : 'brightness_temp_surface',
                     'hfdsnb'    : 'subsurface_heat_flux',
    }

    # unit conversions
    lev2_df["temp_2m"]               = lev2_df["temp_2m"]               + 273.15
    lev2_df["skin_temp_surface"]     = lev2_df["skin_temp_surface"]     + 273.15
    lev2_df["temp_acoustic_mean_2m"] = lev2_df["temp_acoustic_mean_2m"] + 273.15
    MODF.add_citations(var_names=lev2_var_map.keys(), citation='MOSAiC flux group data product, DOI: XXXXXXXXX')

    modf_lev2_df = MODF.map_vars_to_modf(lev2_df, lev2_var_map, drop=True) # map and drop the variables we won't be using

    print(" ... getting seb data")
    lev2_df_10min, data_version = get_flux_data('tower', start_time, end_time, 2, data_dir,  
                                                'seb', nthreads=nthreads, verbose=False, pickle_dir=pickle_dir)

    dm(locals(),0)
    lev2_df_10min = lev2_df_10min[start_time:end_time]

    # lets assume that for a models sake, we can average our flux plates together
    # lev2_df['albedo'] = (lev2_df['down_short_hemisp']/lev2_df['up_short_hemisp'])
    lev2_df_10min['subsurface_heat_flux'] = (lev2_df_10min['subsurface_heat_flux_A'] \
                                           + lev2_df_10min['subsurface_heat_flux_B'] )/2.0

    lev2_10min_var_map = { # now add some 10 minute cadence variables
        'wqv'       : 'Hl', 
        'wthv'      : 'Hs_2m', 
        'hfss_ec'   : 'Hs_10m',
        'hfls_bulk' : 'bulk_Hl_10m',
        'hfss_bulk' : 'bulk_Hs_10m',
    }
    modf_lev2_df_10min = MODF.map_vars_to_modf(lev2_df_10min, lev2_10min_var_map, drop=True) # map and drop the variables we won't be using

    # req ten samples in 10 min period
    mwr_var_map = {
        'prw'   : 'be_pwv',            # column water vapor
        'clwvi' : 'be_lwp',            # column liquid water
        'clb'   : 'cloud_base_height', # cloud altitude above ground
    }
    mwr_df            = get_mosmwrret_data(pool=MODF.pool)
    mwr_mean_args     = [(mwr_df, v, '10min', 30 ) for v in mwr_var_map.values()] # resample at 10min, requiring min 30% good for mean
    mwr_series_list   = MODF.pool.starmap(resample_and_average, mwr_mean_args)
    mwr_resampled     = pd.concat(mwr_series_list, axis=1)
    modf_mwr_df_10min = MODF.map_vars_to_modf(mwr_resampled, mwr_var_map, drop=True) # map then drop vars we won't use

    cloud_hgt_interval = 50
    cloud_ds = get_cloud_profiles(pool=MODF.pool, interval=cloud_hgt_interval)
    cloud_ds = cloud_ds.rename({'nheights' : 'hgt'})
    cloud_var_map = {
        'clt'   : 'Avg_CloudFraction',
        'cllwc' : 'Avg_Retrieved_LWC',
        'cliwc' : 'Avg_Retrieved_IWC',
    }
    cloud_ds_1min_one = MODF.map_vars_to_modf(cloud_ds, cloud_var_map, drop=True) # map then drop vars we won't use
    cloud_ds_1min_two = cloud_ds_1min_one.drop("clt")
    cloud_ds_1min_one = cloud_ds_1min_one.drop(["cllwc","cliwc"])

    # pass off the data to be written and cite the dataset
    MODF.add_data_timeseries(modf_lev2_df       , cadence="time01") #specify the cadence? necessary? or can add_data infer this
    MODF.add_data_timeseries(modf_lev2_df_10min , cadence="time10") #specify the cadence? necessary? or can add_data infer this
    MODF.add_data_timeseries(modf_mwr_df_10min  , cadence="time10") #specify the cadence? necessary? or can add_data infer this

    MODF.add_data_vertical(cloud_ds_1min_one, time_cadence="time01", hgt_interval=f"hgt{cloud_hgt_interval}") 
    MODF.add_data_vertical(cloud_ds_1min_two, time_cadence="time01", hgt_interval=f"hgt{cloud_hgt_interval}") 

    sonde_df_list = get_mosaic_sonde_data(start_time, end_time, data_dir+"../partner_data/AWI/radiosonde/", verbose)
    for sdf in sonde_df_list: # calculate ua and va since it wasn't in file
        sdf["Speed_East"]  = -1*sdf["Speed"]*np.sin(np.deg2rad(sdf["Dir"]))
        sdf["Speed_North"] = -1*sdf["Speed"]*np.cos(np.deg2rad(sdf["Dir"]))

        # sonde units
        sdf["Temp"] = sdf["Temp"] + 273.15 #C to K
        sdf["P"]    = sdf["P"]*100         #hPa to Pa

    sonde_var_map = {  # rename sonde headers to match MODF spec before passing off
        "alt_sonde" : "GpsHeightGnd",
        "pa"        : "P",
        "ta"        : "Temp",
        "hur"       : "RH",
        "ua"        : "Speed_East",
        "va"        : "Speed_North",
        "hus"       : "hus",
    }
    
    print(f"  ... renaming sonde columns, this is intensive for some reason... maybe figure out why?")
    sonde_df_list = MODF.map_vars_to_modf(sonde_df_list, sonde_var_map) #replace in list

    print("  ... adding sonde to MODF and then writing, almost done!!")
    MODF.add_data_sonde(sonde_df_list)

    # then call function that writes the MODF vars to a NetCDF, must go last of course 
    return MODF

    dm(locals(),0)
    MODF.write_files(output_dir='./')


def get_flux_data(station, start_day, end_day, level,
                  data_dir='/Projects/MOSAiC/', data_type='slow',
                  verbose=False, nthreads=1, pickle_dir=None):

    """ Get a dataset from the MOSAiC flux project. 

    Function assumes the standard folder structure as in the NOAA archive. 
    Supports parallelism without using heavy libraries, like dask. 

    Required params
    ---------------
    station     : str station name, used to determine directory and file name, 'asfs30', etc 'tower'
    start_day   : datetime object, used to determine file names
    end_day     : datetime object, used to determine when to stop
    level       : 1, 2, ... which dataset 

    Optional params
    ---------------
    data_dir    : the head where the files can be found
    data_type   : 'fast' 'slow', 'turb','met'data, etc
    nthreads    : how many cpu threads would you like to use 
    verbose     : would you like to see more print statements?

    pickle_dir : if provided, we search for a pandas 'pickle' binary
                 file containing the pre-packed pandas objects. if we
                 don't find it, it is written after ingest. saves *a lot*
                 of time when stored on ramdisk for bigger files.

    Returns
    -------
    tuple (df pandas.DataFrame, str code_version)

             df contains all data between requested days 
             code version is read from file netcdf attributes

    """

    station_list = ['tower','asfs30','asfs40','asfs50']

    if not any(station == name for name in station_list):
        print("\n\nYou asked for a station name that doesn't exist...")
        print("... can't help you here\n\n"); raise IOError

    pickled_filename = f'{station}_{level}_{data_type}_df' # code_version is tacked on to this when run
    df = pd.DataFrame() # dataframe and version we return from this function
    code_version = "?"
    was_pickled = False
    if pickle_dir:
        print(f"\n!!! searching for pickle file containing {pickled_filename} and loading it, takes time...\n")
        for root, dirs, files in os.walk(pickle_dir): # pretty stupid way to find the file
            for filename in files:
                if pickled_filename in filename:
                    filename = pickle_dir+filename
                    df = pd.read_pickle('.//'+filename)
                    name_words = filename.rpartition('_')[-1].rpartition('.')
                    code_version = f"{name_words[0]}.{name_words[1]}"
                    was_pickled = True
                    print(f" ... found and loaded pickle {filename} \n\n")
                    break

        if not was_pickled: print("... didn't find a pickle, we'll write one !!!\n\n")

    if not was_pickled: 
        df_list = [] # data frames get appended here in loop and then concatted by function after
        day_series = pd.date_range(start_day, end_day) 

        q_list = []; p_list = []; day_list = []
        for i_day, today in enumerate(day_series): # loop over days in processing range and get list of files

            if i_day %nthreads == 0:
                if nthreads > 1:
                    print("  ... getting data for day {} (and {} days after in parallel)".format(today,nthreads))
                else:
                    print("  ... getting data for day {}".format(today))

            date_str = today.strftime('%Y%m%d.%H%M%S')
            if level == 1: level_str = 'ingest'
            if level == 2: level_str = 'product'
            if level == 3: level_str = 'archive'

            subdir   = f'/{level}_level_{level_str}_{station}/version2/'
            if station == 'tower':
                subdir   = f'/{level}_level_{level_str}/version2/'
                file_str = f'/mosflx{station}{data_type}.level{level}.{date_str}.nc'
                if level == 2:
                    if data_type=='seb':
                        file_str = f'/mos{data_type}.metcity.level{level}v2.10min.{date_str}.nc'
                    else:
                        file_str = f'/mos{data_type}.metcity.level{level}v2.1min.{date_str}.nc'
                    subdir = subdir+'/'

            else:
                file_str = f'/mos{station}{data_type}.level{level}.{date_str}.nc'
                if level == 2:
                    file_str = f'/mos{data_type}.{station}.level{level}v2.1min.{date_str}.nc'
                    subdir = subdir+'/'

            files_dir = data_dir+station+subdir
            curr_file = files_dir+file_str

            q_today = Q()
            P(target=get_datafile, args=(curr_file, q_today),).start()
            q_list.append(q_today)
            day_list.append(today)
            if (i_day+1) % nthreads == 0 or today == day_series[-1]:
                for iq, qq in enumerate(q_list):
                    df_today = qq.get()
                    cv = qq.get()
                    if cv!=None: code_version = cv # assume all files have same code version, save only one
                    if not df_today.empty: 
                        df_list.append(df_today.copy())
                q_list = []
                day_list = []

        if verbose: print("... concatting, takes some time...")
        try    : df = pd.concat(df_list)
        except : pd.DataFrame()

        try: 
            df.index = df.index.droplevel("freq")
            df       = df[~df.index.duplicated(keep='first')]
        except: pass # this only applices to 'seb'

        time_dates = df.index
        df['time'] = time_dates # duplicates index... but it can be convenient

        if verbose:
            print('\n ... data sample :')
            print('================')
            print(df)
            print('\n')
            print(df.info())
            print('================\n\n') 

        if pickle_dir:
            print("\n\n!!! You requested to pickle the dataframe for speed !!!")
            print(f"... right now we're writing it to this directory {pickled_filename}")
            print("... copy this manually to a ramdisk somewhere for bonus speed points")
            print("... must be symlinked here to be seen by this routine")
            print("...\n... this takes a minute, patience\n\n")
            df.to_pickle(f"{pickle_dir}/{pickled_filename}_{code_version[0:3]}.pkl")

    return df, code_version 


def get_mosaic_sonde_data(start_time, end_time, sonde_dir, verbose=False):

    import glob
    sonde_file_list = glob.glob(sonde_dir+"/*.txt")
    tot_files       = len(sonde_file_list)

    print(f"  ... FOUND {tot_files} sonde files ... opening...")

    col_names = ["HeightGnd","GpsHeightGnd","P","Temp","RH","Dir","Speed"]

    sonde_df_list = []
    for ifile, sfile in enumerate(sonde_file_list):
        with open(sfile, 'rb') as sf: 
            for il, line in enumerate(sf): # get appropriate lines and encode them as ascii strs only
                if il==3: launch_time = line.decode("ascii")
                if il==4: launch_date = line.decode("ascii"); break
        
        dt_str = "".join(launch_time.split())+"-"+"".join(launch_date.split()) # fancy/simple way to strip weird chars
        launch_dt = datetime.strptime(dt_str, "%d/%m/%y-%X") 

        if launch_dt < start_time or launch_dt > end_time:
            tot_files -= 1
            continue
        try:
            sdf = pd.read_csv(sfile, sep='\s+', header=[19,20], encoding = "ISO-8859-1")

        except Exception as e:
        #except UnicodeDecodeError as e:
        #except pd.errors.ParserError as e:
            if verbose:
                print(f"  ... unable to open sonde {sfile}, exception:")
                print(f"  ... {e}")
            continue

        sdf.columns = sdf.columns.get_level_values(0) # remove units from multi-index
        # vaisala time resolution is 1 second... 
        # the mean radiosonde ascent velocity is ~5 m/s.
        dti = pd.date_range(launch_dt, periods=len(sdf), freq="s") # set index to every second
        sdf = sdf.set_index(dti)
        sdf = sdf.apply(pd.to_numeric, errors='coerce') # there are '////' values for nan's in the file 0_o
        sonde_df_list.append(sdf)

    print(f"  ... USED a total of {tot_files}, returning! ")

    return sonde_df_list

# we want to get (when qc_var == 0):
# be_pwv ---> precip water vapor (cm) 
# be_lwp ---> liquid water path  (g/m^2)

def get_mosmwrret_data(mwr_dir='/Projects/MOSAiC_internal/partner_data/ARM/mosmwrret1liljclouM1.c2/', pickle_dir='./', pool=None):
 
    import glob, pickle

    pickled_filename = f'mwr_dfs' # code_version is tacked on to this when run
    df = pd.DataFrame() # dataframe and version we return from this function
    was_pickled = False
    if pickle_dir:
        print(f"\n!!! searching for pickle file containing {pickled_filename} and loading it, takes time...\n")
        for root, dirs, files in os.walk(pickle_dir): # pretty stupid way to find the file
            for filename in files:
                if pickled_filename in filename:
                    filename = pickle_dir+filename
                    mwr_df_complete = pd.read_pickle('.//'+filename)
                    was_pickled = True
                    print(f" ... found and loaded mwrret pickle {filename} \n\n")
                    break
        if not was_pickled: print("... didn't find a pickle, we'll write one !!!\n\n")

    if not was_pickled: 
        print("... getting mwrret data, takes some time")
        mwr_file_list = glob.glob(mwr_dir+"/*.nc")

        arg_list = []
        for i, mwrf in enumerate(mwr_file_list): arg_list.append((mwrf,i))

        if pool is not None: mwr_df_list = pool.starmap(get_datafile_mwr, (arg_list)) 
        else:                mwr_df_list = [get_datafile_mwr(args) for args in arg_list]

        mwr_df_complete = pd.concat(mwr_df_list).sort_index()

        if pickle_dir:
            print("\n\n!!! You requested to pickle the dataframe for speed !!!")
            print(f"... right now we're writing it to this directory {pickled_filename}")
            print("... copy this manually to a ramdisk somewhere for bonus speed points")
            print("... must be symlinked here to be seen by this routine")
            print("...\n... this takes a minute, patience\n\n")
            with open(f"{pickle_dir}/{pickled_filename}_{str(datetime.now()).split()[0]}.pkl", "wb") as pf: 
                pickle.dump(mwr_df_complete, pf)

    return mwr_df_complete

def get_cloud_profiles(clouds_dir='/Projects/MOSAiC/shupeturner/microphysics/variablecoeff/', pickle_dir='./', pool=None, interval=50):
 
    import glob, pickle

    pickled_filename = f'clouds_dfs' # code_version is tacked on to this when run
    cloud_df_list = []
    was_pickled = False
    if pickle_dir:
        print(f"\n... searching for pickle file containing {pickled_filename} and loading it, takes time...")
        for root, dirs, files in os.walk(pickle_dir): # pretty stupid way to find the file
            for filename in files:
                if pickled_filename in filename:
                    filename = pickle_dir+filename
                    clouds_ds = pd.read_pickle('./'+filename)
                    was_pickled    = True
                    print(f" ... found and loaded cloud pickle {filename} \n\n")
                    break
        if not was_pickled: print("... didn't find a pickle, we'll write one !!!\n\n")

    if not was_pickled: 
        print("... getting raw cloud profile data, takes some time")

        clouds_file_list = glob.glob(clouds_dir+"/*.cdf")
        def process_time(ds):
            ds['time'] = ds['time_offset']
            ds['nheights'] = ds['Heights']
            return ds
        clouds_ds = xr.open_mfdataset(f'{clouds_dir}/*.cdf', preprocess=process_time, parallel=True, chunks=0)
        cds = xr.Dataset(clouds_ds.load()) # load to RAM in new dataset, this gets around weird object translation with pickle

        # here we're going to regrid before pickling, so we don't have to constantly recompute the grids as this is
        # pretty slow... let's try to only do that once, as opposed to letting the toolkit do it every time
        hgt_interval = interval
        vars_to_keep = ['Avg_Retrieved_LWC','Avg_Retrieved_IWC','Avg_CloudFraction','Avg_LiqEffectiveRadius']
        new_hgt_grid = [h for h in range(200, 15000+hgt_interval, hgt_interval)]
        regridded_clouds = xr.Dataset(coords={'time': cds.time, 'nheights' : new_hgt_grid})

        # send each variable out in parallel to save a few minutes 
        regrid_args = [(cds[var], regridded_clouds) for var in vars_to_keep]
        if pool is not None: regridded_list = pool.starmap(regrid_xrds_wrapper, (regrid_args)) 
        else:                regridded_list = [regrid_xrds_wrapper(args) for args in regrid_args]
 
        for regridded_data in regridded_list: regridded_clouds[regridded_data.name] = regridded_data
        clouds_ds = regridded_clouds.rename({'nheights' : f'hgt{hgt_interval}', 'time' : 'time01'})

        if pickle_dir:
            print("\n\n!!! You requested to pickle the dataframe for speed !!!")
            print(f"... right now we're writing it to this directory {pickled_filename}")
            print("... copy this manually to a ramdisk somewhere for bonus speed points")
            print("... must be symlinked here to be seen by this routine")
            print("...\n... this takes a minute, patience\n\n")
            with open(f"{pickle_dir}/{pickled_filename}_{str(datetime.now()).split()[0]}.pkl", "wb") as pf: 
                pickle.dump(regridded_clouds, pf)

    return clouds_ds

# must be defined here to be picklable for pool.starmap
def regrid_xrds_wrapper(ds_from, ds_to):
    print(f"... regridding cloud var {ds_from.name}")
    t1 = datetime.now()
    ids =  ds_from.interp_like(ds_to, method='cubic', assume_sorted=True)
    print(f"... {ds_from.name} took {datetime.now()-t1}")
    return ids

# the pandas multi-dimensional index can't be put on queue... somehow... so we wrap it and drop nvbias dim
def get_datafile_mwr(curr_file, i):
    if i % 10 == 0:  print(f"... getting mwr file #{i}!!!")
    mdf = get_datafile(curr_file, i=i)
    mdf.index = mdf.index.droplevel("nvbias").droplevel("nlevels")
    return mdf[~mdf.index.duplicated()]

# simple wrapper to use xarray to convert CF time conventions
# automagically but to return a df because they are nice for timeseries
def get_datafile(curr_file, q=None, i=0, want_df=True):

    if os.path.isfile(curr_file): xarr_ds = xr.open_dataset(curr_file)
    else:
        print(f"!!! requested file doesn't exist : {curr_file}");
        xarr_ds = xr.Dataset(coords={'time':[]}) # empty xarray

    try:
        if want_df:
            data_frame   = xarr_ds.to_dataframe()
            code_version = 'nla'
            q.put(data_frame)
            q.put(code_version)
        else:
            q.put(xarr_ds)

    except Exception as e:
        if want_df: return data_frame # can be implicit but doesn't matter, really
        else: 
            print("returning ds")
            return xarr_ds

# a function that resamples and then averages a dataframe column with a requirement
# on the percentage of points that are non-nan in each period resampled
def resample_and_average(data_frame, var_name, resample_period='10min', percent_required=50):

    # calculate the data points per minute, to then 
    orig_period = np.diff(data_frame.index.values).min().astype(int)/6e10 # native observation period 
    new_period  = float(resample_period.strip('min')) # desired period
    num_points  = np.ceil(np.floor(new_period/orig_period)*(percent_required/100.0))

    def my_mean(series, min_count):
        if len(series) - series.isnull().sum() >=  min_count: return series.mean()
        else: return np.nan

    rsamped_series = data_frame[var_name].resample(resample_period).agg(my_mean, min_count=num_points) 

    return rsamped_series

# ################################################################################################
# actually run the program (allows functions to be defined at bottom) and...
# ... through a (c)python quirk makes things run faster
# 
# https://stackoverflow.com/questions/11241523/why-does-python-code-run-faster-in-a-function
if __name__ == '__main__':

    # first, define some program options/flags, not required but makes command line interaction easier
    # ################################################################################################
    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--start_time',  metavar='str',   help='beginning of processing period, Ymd syntax')
    parser.add_argument('-e', '--end_time',    metavar='str',   help='end  of processing period, Ymd syntax')
    parser.add_argument('-p', '--path',        metavar='str',   help='full data path, including trailing slash') # make portable
    parser.add_argument('-pd', '--pickle_dir', metavar='str',   help='full data path, including trailing slash') # make portable
    parser.add_argument('-v', '--verbose',    action ='count', help='print verbose log messages')
 
    args = parser.parse_args()

    # create a verbose print function 
    if args.verbose: verbose = True
    else: verbose = False
    v_print      = print if args.verbose else lambda *a, **k: None
    verboseprint = v_print

    # paths
    global level1_dir, level2_dir, turb_dir, raise_day 

    if args.path: data_dir = args.path
    elif data_dir==None: data_dir = '/Projects/MOSAiC/'

    if args.pickle_dir: pickle_dir=args.pickle_dir
    elif pickle_dir==None: pickle_dir=False

    level2_dir = data_dir+'/tower/2_level_product/version2/'        # where does level2 data go
    turb_dir   = data_dir+'/tower/2_level_product/version2/'        # where does level2 data go

    #global start_time, end_time # time range we'll process data over
    if args.start_time:
        start_time = datetime.strptime(args.start_time, '%Y%m%d')
    elif start_time==None:
        # make the data processing at beginning of mosaic
        start_time = datetime(2019,10,15)
        start_time = start_time.replace(hour=0, minute=0, second=0, microsecond=0, day=start_time.day)

    if args.end_time:
        end_time = datetime.strptime(args.end_time, '%Y%m%d')
    elif end_time==None:
        end_time = datetime.today() # any datetime object can provide current time
        end_time = end_time.replace(hour=0, minute=0, second=0, microsecond=0, day=start_time.day)
    
    print('  ... the first day we  process data is:     %s' % str(start_time))
    print('  ... the last day we will process data is:  %s' % str(end_time))
    printline(start="\n\n", end="\n")
    time.sleep(3)

    create_mosaic_modf(start_time, end_time, data_dir, pickle_dir, verbose)

