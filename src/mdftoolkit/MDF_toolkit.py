from .MDF_definitions import (
    define_mdf_variables,
    define_mdf_globals,
    define_mdf_all,
    infermdfname
)
import os
import time              # used to fill date_created attribute
import datetime          # create arbitrary time references
import inspect           # provides useful debugging feedback, could eventually be deleted
import sys               # some introspection calls, such as RAM usage etc
import numpy   as np     # a few useful number computing/calculation functions
import netCDF4 as ncdf4  # hard dependency, MDFs are written out as netCDF files
import pandas  as pd     # hard dependency, all the read/write functions assume labeled pd.DataFrame
import xarray  as xr     # hard dependency, all the vertical data is internalized as xr.Datasets

from multiprocessing import Pool

try:
    from debug_functions import drop_me as dm
except ModuleNotFoundError:
    dm = None

if sys.version_info >= (3, 8):
    import importlib.metadata as metadata
else:
    import importlib_metadata as metadata

# Get current version
__version__ = metadata.version('mdftoolkit')


# Helper functions
def _filtervblattrs(attrdict):
    """Filter the variable attributes dictionary before using it.
    """
    return {
        key: str(val) for (key, val) in attrdict.items()
        if key not in ('mdf', 'phase', 'notes', 'minattrs')
        }


# Primary toolkit class
class MDF_toolkit(object):
    """
    An efficient (hopefully) wrapper class designed to encapsulate your data into
    the YOPP siteMIP specified 'MDF' data format (documented/coded in
    MDF_variable_definitions.py). This should be a quick and dirty way to make
    implement the MDF spec that you can use as rails to jump-start your data
    production for use by forecasters/modelers.

    Please do not edit this class to hack together your own solution. If there is
    something you would like to see changed, submit a pull request to the git
    page or e-mail:

        michael.r.gallagher@noaa.gov/mgallagher@awi-polarstern.de

    I tried to keep this code as simple as possible to keep the audience/users as
    broad as possible Please don't abuse the implementation <3

    Parameters:
    ----------
    supersite_name : str, required
        String assigned by the siteMIP data committee that designates the name
        of your site. Is used primarily to name the output file

    verbose : bool, default False
        Flag the provides verbose printing when making method calls and
        creating your own class. Set to True for useful information/feedback
        when designing/debugging

    nthreads : int, default 1
        Number of threads to create when doing a threaded write of the data

    multi_site : bool, default False
        Does your supersite contain multiple disparate locations as part of the same site..?
        If true, sites are made as netcdf groups and variables are allowed to be repeated at each
        site.

    hktabledir: str or pathlib.Path, optional
        The base directory for the HK table files. If None (default) then
        mdftoolkit/HKTable is used.

    hktableversion: str, optional
        The table version to use. This can be a string such as "1.1", in which
        case the filenames are "[file].V1_1.json". It can also be a literal
        string like "V1_1" or "custom". If None (default) then the latest
        version available in basedir is used.

    Example:
    -------
    from MDF_toolkit import MDF_toolkit as MDF
    my_MDF = MDF(supersite_name='my_supersite_name', verbose=True)
    doi_str = 'my data product name, DOI: XXXXXXXXX'

    # read documentation of these functions for details
    my_MDF.update_global_atts (my_global_atts_dict)                 # dictionary providing global site info
    my_MDF.add_data_timeseries(my_mdf_dataframe, cadence="time01") # df columns match MDF definitions
    my_MDF.add_data_vertical  (my_mdf_dataframe, cadence="time01") # df columns match MDF definitions
    my_MDF.add_data_sonde     (my_mdf_dataframe, cadence="time01") # df columns match MDF definitions
    my_MDF.add_data_scalar    ('height_tower', "23.3")              # define some scalar site properties
    my_MDF.add_citations      (my_mdf_dataframe.columns, doi_str)  # cite data DOI
    my_MDF.write_files        (output_dir='./MDF_output/')         # write out to disk

    Assez simple, n'est-ce pas!?

    ================================================================================================
    """

    global isverbose # globals vars used by the class
    global vprint    # global functions used by the class
    isverbose = True # verbose feedback, such as prints

    # constructor, including class properties and their defaults
    def __init__(self, supersite_name: str, *, verbose: bool=False, nthreads: int=1, multi_site: bool=False, enable_mp: bool=False, strict: bool=True, hktabledir=None, hktableversion=None):

        isverbose = verbose

        self._enable_mp = enable_mp
        if enable_mp: self.pool = Pool(nthreads) # a global pool to execute threads on
        else: self.pool = None

        # underscores becaues people shouldn't be modifying/using these directly
        self._verbose           = verbose
        self._strict            = strict
        self._name              = supersite_name
        self._sonde_max_height  = 15000 # meters
        self._sonde_min_height  = 20 # height where 'good' data begins
        self._sonde_alt_res     = 5 # meter, normalized sonde vertical grid
        self._sonde_time_res    = 1 # hour resolution, sonde res for temporal interpolation
        self._multi_site        = multi_site # hour resolution, sonde res for temporal interpolation

        self._sonde_hgt_intervals   = [r for r in range(self._sonde_min_height,
                                                        self._sonde_max_height+self._sonde_alt_res,
                                                        self._sonde_alt_res)]

        # filled with sonde data by self.interpolate_sonde_dataframes()...
        self._sonde_data  = {} # dict of sonde dataframes provided, where key is the first observation in sonde
        self._sorted_sonde_times   = None # "real" sonde launches
        self._sorted_sonde_indexes = None
        self._interped_sonde_times = None # interpolated sonde time series
        self._interped_sonde_dfs   = None

        self._beginning_of_time = datetime.datetime(1970,1,1,0,0,0) # *the* epoch, time offset ints for ncdf files

        # variable attributes and descriptions for the mdf spec
        hkver, mdfglobs, mdfvars = define_mdf_all(
            hktabledir=hktabledir, hktableversion=hktableversion)
        self._global_defs  = mdfglobs
        self._var_defs     = mdfvars
        self._global_atts  = dict()
        self._var_atts     = dict()

        # let's list some basic things that are required ingredients of an MDF
        self._allowed_scalars = [\
                                 "lat",
                                 "lon",
                                 "orog",
                                 "height_radar",
                                 "height_tower",
                                 "depth_iceprobe",
                                 "depth_soilprobe",
                                 ]

        # get a list of available time/cadence options from the variable definitions file, setup
        # var storage select all keys that start with 'time' and have a cadence val
        cadence_list = [dim_name for dim_name in self._var_defs.keys() \
                                 if dim_name.startswith('time') and dim_name[-2:].isnumeric()]
        self._cadence_options = tuple(cadence_list) # tuple, no modifying
        if multi_site: self._subsites = {}
        else:
            self._subsites = {None: subsite(self._cadence_options)}

        hgt_list = [dim_name for dim_name in self._var_defs.keys() \
                    if dim_name.startswith('hgt') and dim_name[-2:].isnumeric()]
        self._hgt_options = tuple(hgt_list) # tuple, no modifying

        # version, date and maintainer
        self._code_version = __version__
        self._defs_version = hkver  # HK table version being used


    # cleanup, no pollution
    def __del__(self):
        if self.pool is not None: self.pool.close()

    def add_subsite(self, subsite_name):
        """

        For a multi-site MDF (specified in the init) name and label the sites. These
        names are then written as group titles in the netcdf files

        Parameters
        ----------
        subsite_name : str
           Name for the site within your supersite that will be the title of the group

        """
        self._subsites[subsite_name] = subsite(self._cadence_options) # this is a dict for now...


    def update_global_atts(self, att_dict: dict):
        """

        Update the global attributes defined by the MDF spec to those
        specific to your site via att_dict.

        Parameters
        ----------
        att_dict : dict
            Dictionary of global attributes for your MDF to update;
            dict keys are the global attribute names (see
            values are type(str) that to set global attribute to

        See also
        --------
        MDF_definitions.define_mdf_globals

        Examples:
        ---------
        MDF = MDF_toolkit(supersite_name='my_supersite_name', verbose=True)
        global_dict = {"contributor_email" : "important_person@important_place.edu"}
        MDF.update_global_atts(global_dict)

        """
        self._global_atts.update(att_dict)

    def _init_variable_atts(self, var_name):
        """Initialize variable attributes from the HK table.
        """
        if var_name in self._var_atts:
            return
        mdfname = infermdfname(var_name, self._var_defs)
        if mdfname is None:
            self._var_atts[var_name] = dict()
        else:
            self._var_atts[var_name] = _filtervblattrs(
                self._var_defs[mdfname])

    def update_variable_atts(self, var_name, att_dict):
        """Update the attributes of a given variable.

        Update the attributes of a variable. If the variable's attributes are
        not yet part of the dictionary, the HK table is used to fill in the
        basic attributes first, which can then be overwritten by the given
        attributes.

        Parameters
        ----------
        var_name: str
            Name of the variable whose attributes are being added. See
            infermdfname to see how the HK table is scanned for basic
            attributes.
        att_dict: dict
            Dictionary of variable attributes to update. Note that these values
            can overwrite those given by the HK table.
        """
        # Initialize if missing
        self._init_variable_atts(var_name)
        # Update the attribute dictionary
        self._var_atts[var_name].update(att_dict)

    def add_data_scalar(self, scalar_name: str, scalar_value, subsite_name: str=None):
        """

        Add value to MDF scalar variables (variables with dimension [0]).

        Parameters
        ----------
        scalar_name : str
            Name of scalar you would like to add, as defined by
            the MDF specification.

        scalar_value : number
            The value you would like to set the

        subsite_name : str
            If you have a 'multi site' MDF, you need to specify the
            name of the subsite that you provided to .add_subsite()

        See also
        --------
        MDF_definitions.define_mdf_scalars

        Examples:
        ---------
        MDF = MDF_toolkit(site_name='my_site_name', verbose=True)
        MDF.add_datascalar('height_tower', 23.3)
        MDF.add_datascalar('height_radar', 5.0)
        MDF.add_datascalar('depth_soilprobe', 1.0)
        MDF.add_datascalar('depth_iceprobe', 3.0)

        """
        # Initialize attributes if missing
        self._init_variable_atts(scalar_name)

        if scalar_name not in self._var_defs:
            _warn("The scalar '{}' you requested to add is not in the MDF specification".format(scalar_name))
        if scalar_name not in self._allowed_scalars:
            _warn("The scalar '{}' you requested to add is not a recognized scalar".format(scalar_name))
        self._subsites[subsite_name].scalar_data[scalar_name] = scalar_value


    def add_citations(self, var_names, citation: str):
        """

        Add data citation to MDF variables, functionally equivalent to "add_var_att" but a little simpler ...

        Parameters
        ----------
        var_names : str, list, dict
            Name(s) of variables that you would like to add
            the citation attribute to

        citation : str
            A string that contains your data citation, typically
            something like a name and a DOI

        Examples:
        ---------
        MDF = MDF_toolkit(site_name='my_site_name', verbose=True)
        vars_to_cite = ['pa' ,'ua' ,'va' ,'ta' ,'tdp','hus']
        citation_str = 'data product, DOI: XXXXXXXXX'
        MDF.add_citations(vars_to_cite, citation_str)

        """
        var_names = (
            [var_names] if isinstance(var_names, str) else list(var_names))
        for var_name in var_names:
            self._init_variable_atts(var_name)
            self._var_atts[var_name]['references'] = citation

    def add_data_timeseries(self, surface_df, cadence: str, subsite_name : str=None, data_id: str="you_didnt_provide_an_identifier"):
        """
        Add data (in DataFrame format) to your MDF where the data columns are
        already named after the MDF variables you would like to store.
        Data must be indexed by pandas.DatetimeIndex at the specified cadence.

        Parameters
        ----------
        surface_df : pandas.DataFrame
            A DataFrame indexed at cadence containing the appropriate data
            in columns with names defined by the MDF specification

        cadence : str
            The cadence of the data provided in surface_df, matching
            the MDF specification.

        subsite_name : str
            If you have a 'multi site' MDF, you need to specifiy which site the data is for by the str
            you provided previously via .add_subsite()

        data_id : str, optional
            A string identifier for the DataFrame primarily used when
            for providing the user feedback  about data quality/specification
            issues. Not used in the writing of the file to disk.

        See also
        --------
        MDF_toolkit.add_df_data
        MDF_definitions.define_mdf_variables

        Caveats
        -------
        Currently for the specified cadence (such as 'time20') (every 20 mins)
        the code expects data to only be located at intervals of 20
        minutes after the first data point, for all time. so you can't
        have the data begin at midnight on Oct 1st 2010 and then
        suddenly have data change to 20 min intervals starting at 12:10
        on Oct 1st 2016 or something. This is annoying, but also not easily
        solved. Hopefully it's not to constraining and it can likely be
        improved in the near-future.

        """

        if cadence not in self._cadence_options: # error out on bad requests
            print(f"... you chose a cadence for your data that was not part of the spec, {cadence}")
            print(f"... please change your data to one of the following cadences:\n {_str_list_to_str(self._cadence_options)}")
            raise DataSpecError("cadence")

        if type(surface_df.index) != pd.core.indexes.datetimes.DatetimeIndex:
            _warn("Right now it's required that you provide a dataframe with a time-based index")
            raise DataSpecError("index type")

        surface_df = MDF_toolkit._check_and_drop_vars(surface_df, data_id, strict=self._strict)
        self._subsites[subsite_name].var_data[cadence].append(surface_df)

        return

    def add_data_vertical(self, vert_data: xr.Dataset, time_cadence: str,  hgt_interval: str, data_id: str="you_didnt_provide_id"):
        """

        Add vertical data to your MDF. Function accepts individual xr.Datasets or
        a list of Datasets This function expects that the columns are pre-named to
        follow the MDF spec (use map_vars_to_mdf if you'd like).  Dataset must contain
        proper height and time coordinates.

        Parameters
        ----------
        vert_data : xr.Dataset || [xr.Dataset,]
            A individual or list of Dataset containing the appropriate data
            in columns with names defined by the MDF specification

        time_cadence : str
            String matching one of the MDF specced cadences that the provided
            data is mapped to. (i.e. time01, etc. et. al)

        hgt_interval : str
            String matching one of the MDF specced cadences that the provided
            data is mapped to. (i.e. hgt10, etc. et. al)

        data_id : str, optional
            A string identifier for the DataFrame primarily used when
            for providing the user feedback about data quality/specification
            issues. Not used in the writing of the file to disk.

        See also
        --------

        """

        # allow people to add individual dfs if they want, be a little flexible
        if type(vert_data) != type([]): vert_data = [vert_data]

        if time_cadence not in self._cadence_options: # error out on bad requests
            print(f"... you chose a cadence for your data that was not part of the spec, {time_cadence}")
            print(f"... please change your data to one of the following cadences:\n {_str_list_to_str(self._cadence_options)}")
            raise DataSpecError("cadence")

        if hgt_interval not in self._hgt_options: # error out on bad requests
            print(f"... you chose a height interval for your data that was not part of the spec, {hgt_interval}")
            print(f"... please change your data to one of the following intervals:\n {_str_list_to_str(self._hgt_options)}")
            raise DataSpecError("hgt")

        vert_data = MDF_toolkit._check_and_drop_vars(vert_data, data_id, strict=self._strict)

        try: self._vert_data[time_cadence]
        except KeyError: self._vert_data[time_cadence] = []

        for ds in vert_data:
            if type(ds) != type(xr.Dataset()):
                raise DataSpecError("for now only xarray datasets are accepted for vertical data")

            try: ds[time_cadence]
            except KeyError:
                try: ds = ds.rename({'time' : time_cadence})
                except: raise DataSpecError(f"dataset must contain 'time' or {time_cadence} coords")

            try: ds[hgt_interval]
            except KeyError:
                try: ds = ds.rename({'hgt' : hgt_interval})
                except: raise DataSpecError(f"dataset must contain 'time' or {hgt_interval} coords")

            self._vert_data[time_cadence].append(ds)

        return

    def add_data_sonde(self, sonde_data, data_id: str="you_didnt_provide_an_identifier"):
        """

        Add sonde data to your MDF. Function accepts individual sonde DataFrames or
        a list of DataFrames, where the format of the dataframe is as follows. This
        function expects that the columns are pre-named to follow the MDF spec.
        DataFrame must be indexed by pandas.DatetimeIndex.

        Parameters
        ----------
        sonde_data : pandas.DataFrame || [pandas.DataFrame]
            A individual or list of DataFrame containing the appropriate data
            in columns with names defined by the MDF specification

        data_id : str, optional
            A string identifier for the DataFrame primarily used when
            for providing the user feedback about data quality/specification
            issues. Not used in the writing of the file to disk.

        See also
        --------
        MDF_definitions.define_mdf_variables

        Caveats
        -------

        """

        # allow people to add individual dfs if they want, be a little flexible
        if type(sonde_data) != type([]): sonde_data = [sonde_data]

        # filter var_defs and by keys with "sonde" str", except time, create dict
        # sonde_vars = dict(filter( lambda key: \
        #                           "sonde" in key[0] if "time" not in key[0] else None,
        #                           mdf_vars.items()))

        # right now this function would hypothetically accept as sonde data, MDF vars that
        # are not sonde vars. a check should be made using the above array?

        sonde_df = MDF_toolkit._check_and_drop_vars(sonde_data, data_id, strict=self._strict)
        for sonde_df in sonde_data:
            self._sonde_data[sonde_df.index[0]] = sonde_df

        return

    def write_files(self, output_dir: str='./', fname_only_underscores: bool=False):
        """

        After creating the MDF, adding attributes and citations, and adding data, you
        can finally ask the toolkit to write the MDF to disk. This is the function that does
        a lot of the heavy lifting for the toolkit user. It's also consequentially the
        place where you're likely to find the most bugs.

        For example, in this first beta, this write_files function expects you to have
        added your DataFrames to the MDF sequentially in time. It makes no account if you
        have added DataFrames column by column. There is also no error checking for this
        at the moment. I plan to add this functionality ASAP of course.

        Parameters
        ----------
        output_dir : str
            The relative directory where you would like to the MDF
            netCDF output data to be written
        fname_only_underscores : bool
            If True, only use underscores to separate fields (e.g. dates) in the
            output filename. The default is False.

        """

        # this function makes lots of assumptions on the data stored, that data has been appropriately
        # filtered/checked/cadenced prior to being called and that now everything can be written
        # ########################################################################################

        # if sonde data exists, interpolate sonde dataframes, this function does a lot of heavy lifting,
        # but the summary is, it takes the list of dataframes provided by the user and normalizes them to
        # a time height grid *but* marks the "real" provided sonde profiles to distinguish between interpolated
        # and observed data
        if self._sonde_data and self._interped_sonde_dfs is None: # check to see if sondes already processed by user
                self.interpolate_sonde_dataframes()               # if no, this function, fills this^ class variable

        # now we process and write the scalar/timeseries data:
        # ########################################################################################
        # sort surface (i.e.) dataframes stored in self._var_data and prepares for writing the files,
        # here we all dataframes of the same cadence and find when the beginning
        # and end of the MDF will be, based on the data provided to var_data by the user
        data_by_cadence       = {}
        vert_by_cadence       = {}
        start_date_by_cadence = {}
        end_date_by_cadence   = {}
        for subsite in self._subsites.keys():
            data_by_cadence[subsite]       = {}
            vert_by_cadence[subsite]       = {}
            start_date_by_cadence[subsite] = {}
            end_date_by_cadence[subsite]   = {}
            for cadence in self._cadence_options:

                # check and concatenate time series in this cadence
                try:
                    for vdf in self._subsites[subsite].var_data[cadence]:
                        if vdf.index.duplicated().sum() > 0:
                            _warn(f"Data in {cadence} had duplicated index/obs, we'll de-duplicate but you should " \
                                  +"probably check that this does what you want and there's not a problem")
                            vdf = vdf[vdf.index.duplicated(keep='first')]

                    # concat all user provided dataframes at same cadence for writing and sort
                    print(self._subsites[subsite].var_data[cadence])
                    data_by_cadence[subsite][cadence] = pd.concat( self._subsites[subsite].var_data[cadence], axis=1 )
                    data_by_cadence[subsite][cadence] = data_by_cadence[subsite][cadence].sort_index()

                    # find start and end date for file boundaries
                    start_date_by_cadence[subsite][cadence] = data_by_cadence[subsite][cadence].index[0]
                    end_date_by_cadence  [subsite][cadence] = data_by_cadence[subsite][cadence].index[-1]
                    first_date = start_date_by_cadence[subsite][cadence] # set start/end date to last available cadence data, then
                    last_date  = end_date_by_cadence[subsite][cadence]   # we will check

                except ValueError as e:
                    data_by_cadence[subsite][cadence] = pd.DataFrame()
                    vprint("  ... there was no time series data provided for {} cadence".format(cadence))

                # check and put vertical data in this cadence
                try:

                    # concat all user provided dataframes at same cadence for writing and sort
                    vert_by_cadence[subsite][cadence] = xr.merge(self._subsites[subsite].vert_data[cadence])
                    vert_by_cadence[subsite][cadence] = vert_by_cadence[subsite][cadence].sortby(cadence)
                    vprint("  ... using vertical data for {} cadence".format(cadence))
                except KeyError as e:
                    vert_by_cadence[subsite][cadence] = xr.Dataset()

            for cadence in self._cadence_options:
                try:
                    if start_date_by_cadence[subsite][cadence] < first_date: first_date = start_date_by_cadence[subsite][cadence]
                    if end_date_by_cadence[subsite][cadence]   > last_date : last_date  = end_date_by_cadence[subsite][cadence]
                except: do_nothing = True # cadence has no data, already warned user above, doing nothing

            print(subsite)

        # print some informative statistics on RAM usage and data to be saved
        printline(start='\n', end='\n')
        ram_usage = 0;
        for subsite in self._subsites.keys():
            for df in data_by_cadence[subsite].values()  : ram_usage += df.memory_usage().sum()
            for ds in vert_by_cadence[subsite].values()  : ram_usage += ds.nbytes

        for df in self._sonde_data.values() : ram_usage += df.memory_usage().sum()
        self._ram_usage = ram_usage
        ram_str = _bytes_to_hr(ram_usage)

        vprint(f"Your data is currently taking {ram_str} of RAM and")
        vprint("based on what you provided, observations will be used from:\n")
        vprint(f"------------->      {first_date}    UNTIL    {last_date}\n")
        if isverbose: printline(end='\n')
        time.sleep(3) # we want this to be seen

        # ########################################################################################
        # we have all the timeseries and sondes processed, now we write this data to disk
        # accodringly, with the appropriate dimensions, variables, and attributes
        if fname_only_underscores:
            mdf_file_str  = "./"+self._name+'_MDF_'+first_date.strftime('%Y%m%d')+'_'+last_date.strftime('%Y%m%d')+'.nc'
        else:
            mdf_file_str  = "./"+self._name+'_MDF_'+first_date.strftime('%Y%m%d')+'-'+last_date.strftime('%Y%m%d')+'.nc'
        mdf_file_out = output_dir+mdf_file_str
        if os.path.exists(mdf_file_out):
            _warn(f"Overwriting old file:\n {mdf_file_out}"); time.sleep(5)
            os.remove(mdf_file_out)
        mdf_ncdf = ncdf4.Dataset(mdf_file_out, 'w', mmap=False)

        # check global atts, make sure they are provided by the user and write them to the ncdf file
        self.update_global_atts({"date_created": "{}".format(time.ctime(time.time())), 'featureType': 'timeseries'})
        not_updated = list(
            set(
                attr for (attr, data) in self._global_defs.items()
                if data['mdf'] != 'MMDF')
            - set(
                attr for (attr, val) in self._global_atts.items()
                if val not in (None, '')))
        if len(not_updated):
            _warn(f"... you did not provide/update some global variables required by the MDF specification {_str_list_to_str(not_updated)}")
            # raise DataSpecError(f"global variable(s) :\n{_str_list_to_str(not_updated)}")

        for att_name, att_val in self._global_atts.items(): # write global atts to file
            if att_val is not None: mdf_ncdf.setncattr(att_name, att_val)

        for subsite in self._subsites.keys():
            if subsite is None: ncdf = mdf_ncdf
            else: ncdf = mdf_ncdf.createGroup(subsite)
            for cadence in self._cadence_options: # loop over datasets at each time cadence

                if not data_by_cadence[subsite][cadence].empty:
                    cadence_data  = data_by_cadence[subsite][cadence]
                    cadence_range = pd.date_range(first_date, last_date, freq='{}T'.format(cadence.strip('time')))
                    cadence_data  = cadence_data.reindex(labels=cadence_range, copy=False) # reindex and fill with nans

                    # create the arrays that are integer intervals from 'time since first obs' for indexing
                    bot        = self._beginning_of_time
                    dti        = pd.DatetimeIndex(cadence_data.index.values)
                    delta_ints = np.floor((dti - bot).total_seconds()/60.0) # minutes since first obs
                    t_ind      = pd.Int64Index(delta_ints)

                    # set the time dimension and matching variable attributes to what's defined above
                    ncdf.createDimension(cadence , None)
                    t = ncdf.createVariable(cadence, 'uint64', cadence)
                    t[:] = t_ind.values

                    time_atts = _filtervblattrs(self._var_defs[cadence])
                    time_atts["units"] = "minutes since {}".format(bot)
                    for att_name, att_val in time_atts.items(): ncdf[cadence].setncattr(att_name, att_val)

                    print("You have provided timeseries for the following MDF variables at cadence {}: ".format(cadence))
                    var_list = list(cadence_data.columns)
                    print(f"{_str_list_to_str(var_list)}\n")
                    print("Those are the variables you will find in your MDF")
                    printline(start="\n", end="\n")

                    for var_name in var_list:      # loop over vars in cadence_data and write them to the file
                        if var_name not in self._var_defs.keys():
                            _warn("something went really wrong, you should never make it here...")
                            raise DataSpecError(f", variable {var_name} is not in MDF spec ")

                        var = ncdf.createVariable(var_name, 'float64', cadence, zlib=True)
                        self._init_variable_atts(var_name)
                        for att_name, att_desc in self._var_atts[var_name].items(): var.setncattr(att_name, att_desc)

                        fill_val = ncdf4.default_fillvals['f8']
                        fill_val_uint = ncdf4.default_fillvals['u8']

                        maxvar = np.nanmax(cadence_data[var_name])
                        minvar = np.nanmin(cadence_data[var_name])
                        avgvar = np.nanmean(cadence_data[var_name])
                        perc_miss = _perc_missing(cadence_data[var_name])

                        var[:] = cadence_data[var_name].fillna(fill_val).values

                        ncdf[var_name].setncattr('max_val', maxvar)
                        ncdf[var_name].setncattr('min_val', minvar)
                        ncdf[var_name].setncattr('avg_val', avgvar)
                        ncdf[var_name].setncattr('percent_missing', perc_miss)
                        ncdf[var_name].setncattr('missing_value', fill_val)

                    if len(vert_by_cadence[subsite][cadence].variables) ==0:
                        vert_var_list = []
                    else:
                        vert_cadence_data  = vert_by_cadence[subsite][cadence]
                        vdc  = vert_cadence_data.reindex({cadence : cadence_range}) # reindex and fill with nans

                        vert_var_list = [v for v in vdc.variables if not any(s in v for s in ['time', 'hgt'])]

                        for var_name in vert_var_list:      # loop over vars in cadence_data and write them to the file

                            # get vertical dimension name, and if necessary create dimension and assign values
                            if len(vdc[var_name].coords) > 2: raise DataSpecError(f"variable {var_name} had more than two dimensions")
                            vert_dim = [c for c in vdc[var_name].coords if 'hgt' in c][0] #hgt xXx
                            if vert_dim not in self._var_defs.keys():
                                _warn(f"vertical variable {var_name} had an unknown/unspecced height dimension")
                                continue
                            else:
                                try: mdf_ncdf[vert_dim] # check if exists
                                except:
                                    mdf_ncdf.createDimension(vert_dim , size=len(vdc[vert_dim]))
                                    v = mdf_ncdf.createVariable(vert_dim, 'uint64', vert_dim)
                                    v[:] = np.int64(vdc[vert_dim].values)
                                    att_dict = _filtervblattrs(self._var_defs[vert_dim]) # get attributes for var from spec and write them
                                    for att_name, att_desc in att_dict.items(): v.setncattr(att_name, att_desc)

                            var = mdf_ncdf.createVariable(var_name, 'float64', (cadence, vert_dim), zlib=True)
                            self._init_variable_atts(var_name)
                            for att_name, att_desc in self._var_atts[var_name].items(): var.setncattr(att_name, att_desc)

                            maxvar = np.nanmax(vdc[var_name])
                            minvar = np.nanmin(vdc[var_name])
                            avgvar = np.nanmean(vdc[var_name])
                            perc_miss = _perc_missing(vdc[var_name])

                            var[:,:] = vdc[var_name].fillna(fill_val).values

                            mdf_ncdf[var_name].setncattr('max_val', maxvar)
                            mdf_ncdf[var_name].setncattr('min_val', minvar)
                            mdf_ncdf[var_name].setncattr('avg_val', avgvar)
                            mdf_ncdf[var_name].setncattr('percent_missing', perc_miss)
                            mdf_ncdf[var_name].setncattr('missing_value', fill_val)

                    print("You have provided timeseries for the following MDF variables at cadence {}: ".format(cadence))
                    print(f"{_str_list_to_str(list(var_list)+list(vert_var_list))}\n")
                    print("Those are the variables you will find in your MDF")
                    printline(start="\n", end="\n")

        mdf_ncdf.close() # close and write files for today

        # ##########################################
        # Next we process and write the sonde data:
        if self._sonde_data:
            if fname_only_underscores:
                sonde_file_str  = "./"+self._name+'_MDF_sondes_'+first_date.strftime('%Y%m%d')+'_'+last_date.strftime('%Y%m%d')+'.nc'
            else:
                sonde_file_str  = "./"+self._name+'_MDF_sondes_'+first_date.strftime('%Y%m%d')+'-'+last_date.strftime('%Y%m%d')+'.nc'
            sonde_file_out = output_dir+sonde_file_str
            if os.path.exists(sonde_file_out):
                _warn(f"Overwriting old file:\n {sonde_file_out}"); time.sleep(5)
                os.remove(sonde_file_out)
            sonde_ncdf = ncdf4.Dataset(sonde_file_out, 'w', mmap=False)

            for att_name, att_val in self._global_atts.items(): # write global atts to sonde file
                val = att_val.get('value', None)
                if val is not None: sonde_ncdf.setncattr(att_name, att_val)

            # get the original time grid in these two vars to mark "real" sonde launches
            sorted_times = self._sorted_sonde_times; sorted_indexes = self._sorted_sonde_indexes

            # and the interpolated grid here:
            interped_time_index = self._interped_sonde_times; interped_sonde_dfs = self._interped_sonde_dfs

            # ######################################################################
            #  back to the real world! we mark the times that actual sondes were
            #  l(a)unched so that we can back out what happened in reality vs
            lunch_time_dti  = pd.DatetimeIndex(sorted_times.values)
            real_sondes_dti = lunch_time_dti.round(freq="H")
            real_inds       = [i for i,d in enumerate(interped_time_index) if d in real_sondes_dti]
            # thus these are indexes from time-interp results that match up with *real* launches

            # ###################################################################
            # hooray, we have everything we need, we can write it all to the
            # file now! let's start by setting up the time and height coordinates
            # then we'll loop over the individual sonde variables

            # put sorted times into sonde ncdf file dimension and variable
            bot                = np.datetime64(self._beginning_of_time)
            release_dti        = pd.DatetimeIndex(sorted_times.values)
            release_deltas     = np.floor((release_dti - bot).total_seconds())

            # these are the times of *actual* releases, set to missing if interped
            release_time_deltas = pd.Series([fill_val_uint]*len(interped_time_index))
            for i, i_interped in enumerate(real_inds):
                release_time_deltas.iloc[i_interped] = np.int64(release_deltas[i])

            # these are the 'valid' times, the nearest hour to launch, and will
            # be set to a time whether interped or no
            valid_time_deltas = np.floor((interped_time_index - bot).total_seconds())

            # but now we have to match those indexes with the interpolated grid
            print(f"number of 'real' sondes: {len(real_inds)} vs number of interped: {len(release_time_deltas)}")
            printline()

            print("... finally writing sondes to disk !!!")
            # now save release time and valid time to file variables
            # *and* make the valid time, "time_sonde", the fundamental dimension
            sonde_ncdf.createDimension('time_sonde_valid', None)
            ts    = sonde_ncdf.createVariable('time_sonde_valid', 'uint64', 'time_sonde_valid')
            ts[:] = valid_time_deltas.values

            tr    = sonde_ncdf.createVariable('time_sonde_released', 'uint64', 'time_sonde_valid')
            tr[:] = release_time_deltas.values

            for att_name, att_val in _filtervblattrs(self._var_defs['time_sonde_valid']).items(): sonde_ncdf['time_sonde_valid'].setncattr(att_name, att_val)
            sonde_ncdf['time_sonde_valid'].setncattr('units', f'seconds since {bot}')
            sonde_ncdf['time_sonde_valid'].setncattr('missing_value', fill_val_uint)

            for att_name, att_val in _filtervblattrs(self._var_defs['time_sonde_released']).items(): sonde_ncdf['time_sonde_released'].setncattr(att_name, att_val)
            sonde_ncdf['time_sonde_released'].setncattr('units', f'seconds since {bot}')
            sonde_ncdf['time_sonde_released'].setncattr('missing_value', fill_val_uint)

            sonde_vert_dim  = 'alt_sonde'
            sonde_hgt_intervals = self._sonde_hgt_intervals
            sonde_ncdf.createDimension(sonde_vert_dim, size=len(sonde_hgt_intervals))
            sh    = sonde_ncdf.createVariable(sonde_vert_dim, 'uint64', sonde_vert_dim)
            sh[:] = np.int64(sonde_hgt_intervals)
            for att_name, att_val in _filtervblattrs(self._var_defs[sonde_vert_dim]).items(): sonde_ncdf[sonde_vert_dim].setncattr(att_name, att_val)

            # finally vertical dimension and variables sonde params to put the data into
            sonde_fill_val = ncdf4.default_fillvals['f8']
            sonde_var_dict = {}
            perc_miss_dict = {}

            sonde_columns = interped_sonde_dfs[0].columns
            # initialize sonde netcdf variables before filling them with data
            for svar_name in sonde_columns:
                if svar_name not in self._var_defs.keys():
                    _warn(f"... sonde var {svar_name} not found in HK table")
                    # continue

                try: # to create variable
                    new_svar = sonde_ncdf.createVariable(svar_name, 'float64', ('time_sonde_valid', sonde_vert_dim), zlib=True)
                    sonde_var_dict[svar_name] = new_svar # if succesful
                    self._init_variable_atts(svar_name)
                    for att_name, att_val in self._var_atts[svar_name].items(): sonde_ncdf[svar_name].setncattr(att_name, att_val)
                    perc_miss_dict[svar_name] = 0

                except Exception as e: # except if variable has already been created
                    _warn(f" Something went wrong adding var {svar_name}")
                    raise

            # if this all works, you only need to re-obtain individual sonde frames
            # and then write those to the netcdf file, making sure times for interpolated 'sondes' are nan
            for idf, sonde_df in enumerate(interped_sonde_dfs):
                #print(f"... adding sonde for time: {release_dti[idf]}")
                if idf % 500 == 0: vprint(f"... adding sonde {idf} of {len(interped_sonde_dfs)}")

                # this will throw exception if a sonde_df has different columns
                for svar_name in sonde_columns:
                    if svar_name not in self._var_defs.keys(): continue # you've already been warned
                    sv_data                        = sonde_df[svar_name]
                    perc_miss_dict[svar_name]     += _perc_missing(sv_data)
                    sonde_var_dict[svar_name][idf] = np.float64(sv_data.fillna(sonde_fill_val).values)
                    # save values to disk

            for svar_name in sonde_columns:# loop over sonde vars actually written to file
                if svar_name not in self._var_defs.keys(): continue
                perc_miss = perc_miss_dict[svar_name]/float(idf)
                sonde_ncdf[svar_name].setncattr('percent_missing', perc_miss)
                sonde_ncdf[svar_name].setncattr('missing_value', sonde_fill_val)


            # #################################################################
            # add boundary layer heights to sonde files, this is present in both
            pbl_vars = ['zmla_ri_grad', 'zmla_temp_grad']
            pbl_df = self._h_pbl_df
            # initialize sonde netcdf variables before filling them with data
            for svar_name in pbl_vars:
                if svar_name not in self._var_defs.keys():
                    _warn(f"... pbl var {svar_name} is not in the HK table")
                    # continue

                try: # to create variable
                    sv_data = pbl_df[svar_name]
                    new_svar = sonde_ncdf.createVariable(svar_name, 'float64', 'time_sonde_valid', zlib=True)
                    new_svar[:] = np.float64(sv_data.fillna(sonde_fill_val).values)
                    sonde_ncdf[svar_name].setncattr('percent_missing', _perc_missing(sv_data))
                    sonde_ncdf[svar_name].setncattr('missing_value', sonde_fill_val)
                    self._init_variable_atts(svar_name)
                    for att_name, att_val in self._var_atts[svar_name].items(): sonde_ncdf[svar_name].setncattr(att_name, att_val)

                except Exception as e: # except if variable has already been created
                    _warn(f" Something went wrong adding var {svar_name}")
                    raise

        printline()
        print("\n\n Wrote your MDF files to the following locations: ")
        print(f"    {mdf_file_out}")
        if self._sonde_data:
            print(f"    {sonde_file_out}\n\n")
        printline()

        try: sonde_ncdf.close() # close and write files for today
        except: throws_exception_if_no_sonde_data = True


    @staticmethod
    def map_vars_to_mdf(data_list, name_map_dict: dict, drop: bool=False, subsite_name: bool=None, hktabledir=None, hktableversion=None):
        """

        Map DataFrame columns to names in the MDF specification by key value
        pair in the provided dictionary. Values are the DataFrame variable you
        would like to rename and keys are the MDF variable you would like
        to rename them to.

        Returns
        -------
        [pd.DataFrame] or pd.DataFrame containing the original data but with columns appropriately
        renamed along the MDF definitions. Specify drop=True if you would
        like the columns intentionally not provided in name_map_dict to be dropped
        before returning the DataFrame.

        Parameters
        ----------
        data_list : [pd.DataFrame || xr.Dataset]
            A DataFrame (or list of DataFrames) indexed with
            appropriate data in columns with names you would like to change

        name_map_dict : dict
            A dictionary that maps MDF variable names (dict keys) to the
            DataFrame column names (dict values).

        drop : bool, optional
            Specify whether you would like the returned DataFrame to
            contain the data columns that you did not specify to be
            renamed to the MDF specification.

        hktabledir: str or pathlib.Path, optional
            The base directory for the HK table files. If None (default) then
            mdftoolkit/HKTable is used.

        hktableversion: str, optional
            The table version to use. This can be a string such as "1.1", in
            which case the filenames are "[file].V1_1.json". It can also be a
            literal string like "V1_1" or "custom". If None (default) then the
            latest version available in basedir is used.

        See also
        --------
        MDF_toolkit.add_df_data
        MDF_definitions.define_mdf_variables

        """
        if type(data_list) != type([]):
            data_list = [data_list] # let people pass single frame/set
            singleton = True
        else: singleton = False

        mdf_vars     = define_mdf_variables(
            hktabledir=hktabledir, hktableversion=hktableversion)
        new_col_names = {}
        wrong_names   = [] # holds variable names that don't exist in the spec but that were asked to write
        nodata_vars   = [] # hold variable names that don't exist in the data but were asked to write
                           # ... mostly typo protection

        new_data_list = []
        for data in data_list:
            orig_data = data
            if type(data) == type(xr.Dataset()):
                xarr = True
                data_cols = list(data.variables)
            else:
                data_cols = data.columns
                if type(data.index) != pd.core.indexes.datetimes.DatetimeIndex:
                    _warn("Right now it's required that you provide dataframes with a DatetimeIndex"); raise DataSpecError("index type")
                xarr = False

            if data is data_list[0]:
                for new_name, old_name in name_map_dict.items():
                    if new_name in mdf_vars     : new_col_names[old_name] = new_name
                    else                         : wrong_names.append(new_name)
                    if old_name not in data_cols : nodata_vars.append(old_name)

                if len(wrong_names):
                    _warn("You asked to rename some vars to the MDF spec but they were not part of the spec:\n"
                          f"    {_str_list_to_str(wrong_names)}")
                if len(nodata_vars):
                    _warn("The data you provided is doesn't have variable you said it would... \n"
                          f"{_str_list_to_str(nodata_vars)}")

            if xarr: data = data.rename(new_col_names)
            else: data = data.rename(columns=new_col_names)

            if drop:
                if type(data) == type(xr.Dataset()): cols_left = list(data.variables)
                else: cols_left = data.columns

                drop_cols = [col for col in cols_left if col not in name_map_dict.keys()]
                if xarr: data = data.drop(drop_cols)
                else: data = data.drop(columns=drop_cols)

            # copy the coordinates, didn't find a function that did this...
            if xarr:
                data = data.assign_coords(orig_data.coords)
            #for coord in orig_data.coords: data[coord] = orig_data[coord]

            new_data_list.append(data)

        if singleton:
            return new_data_list[0]
        else: return new_data_list

    def check_file_for_spec(filename):
        """
        this function checks a NetCDF file to make sure that it matches the MDF spec...
        ... or it will... but it doesn't yet.
        """
        _warn("Not implemented yet, but this will be a function that checks files created by other people/methods...")

    def load_mdf(filename):
        """
        This function reads an MDF file from disk into memory.
        Use at your own risk, has not been rigorously tested.

        Returns
        -------
        MDF_toolkit object read from the given filename

        Parameters
        ----------
        filename : str
            Path to read in MDF file at

        """

        # Check that file matches spec
        MDF_toolkit.check_file_for_spec(filename)

        # Subsites not implemented yet...
        if len(ncdf4.Dataset(filename).groups) > 0:
            raise NotImplementedError("Reading MDF with subsites (NetCDF groups) not implemented yet")

        # Load MDF as xarray dataset
        ds = xr.open_dataset(filename)

        # Create MDF object for output
        # parse supersite name from filename
        fn = os.path.split(filename)[1]
        name = fn[:fn.find('_MDF_')]
        MDF_out = MDF_toolkit(supersite_name=name)

        # Set the global attributes from the DataSet
        MDF_out.update_global_atts(ds.attrs)

        # Get cadences
        cadences = set()
        for key in ds.dims.keys():
            if key[:4]=='time':
                cadences.add(key)

        # Get variable names for each timeseries
        ts_dict = {}
        for cadence in cadences:
            ts_dict[cadence] = []
        for var in ds.data_vars:
            if len(ds[var].dims) == 1:
                if ds[var].dims[0] in ts_dict:
                    ts_dict[ds[var].dims[0]].append(var)

        # Get dataframes from DataSet for each cadence
        # And add to the MDF
        for cadence in ts_dict.keys():
            df = ds[ts_dict[cadence]].to_dataframe()
            MDF_out.add_data_timeseries(df, cadence=cadence)
        
        return MDF_out

    # when finished, replace sonde_data with self._sonde_data.
    def interpolate_sonde_dataframes(self):

        """""

        This function takes all of the supplied time-indexed individual sonde
        frames, and regrids each sonde to the specced height grid, while
        interpolating in time to the specced time resolution. It is called in the
        sonde write file code but can be called by the user if the data is required
        by the the user before writing.

        """

        # get all the sonde release times and dataframes from internal storage
        release_times = []; sonde_dfs = []
        for release_time, sonde_df in self._sonde_data.items():
            release_times.append(release_time); sonde_dfs.append(sonde_df)

        # cheap hack to use DataFrame object to sort our sonde data by launch datetime for us
        lt_df = pd.DataFrame(release_times); lt_df = lt_df.set_index(lt_df[0])

        sorted_times, sorted_indexes = lt_df.index.sort_values(return_indexer=True)
        sorted_sonde_dfs = [sonde_dfs[i] for i in sorted_indexes]
        sonde_columns    = sorted_sonde_dfs[0].columns
        sonde_columns    = list(sonde_columns.drop('alt_sonde'))
        # take care of altitude in our interpolation, not needed in columns

        # ###########################################################################
        # here things will get a little wacky, we need to do some processing of the
        # provided sonde dataframes before we do any netcdf writing, first we will
        # grid each sonde df to the height grid specified by the spec, then we'll
        # aggregate each variable from each dataframe and interpolate in time to
        # meet the time interpolation spec....

        # convert each sonde to uniform height grid,
        # now make a dictionary that contains a time*height df for each variable in parallel via pool.starmap
        vprint("... regridding sonde profiles to uniform height grid")
        sonde_hgt_intervals = self._sonde_hgt_intervals
        if self._enable_mp:
            regrid_args         = [(sonde_df.copy(), sonde_hgt_intervals) for sonde_df in sorted_sonde_dfs]
            regridded_sonde_dfs = self.pool.starmap(MDF_toolkit._regrid_sonde_heights, regrid_args) # call regrid
        else:
            regridded_sonde_dfs = [MDF_toolkit._regrid_sonde_heights(sonde_df.copy(), sonde_hgt_intervals) for sonde_df in sorted_sonde_dfs]

        # here we combine into height X time grids for each variable in parallel via pool.starmap
        vprint("... combining sondes profiles into single frames for time interpolation ")
        if self._enable_mp:
            combine_args   = [(regridded_sonde_dfs, sonde_hgt_intervals, sv) for sv in sonde_columns]
            regrid_results = self.pool.starmap(MDF_toolkit._combine_sondes, combine_args) # returns list(dicts)
        else:
            regrid_results = [MDF_toolkit._combine_sondes(regridded_sonde_dfs, sonde_hgt_intervals, sv) for sv in sonde_columns]

        sonde_df_dict = {} # this becomes a dictionary, with keys for sonde vars and
                           # values that are launch_time vs. height dataframes
        for res in regrid_results: sonde_df_dict = {**sonde_df_dict, **res}

        # and now we finally interp in Xhourly time before actually writing the files! in parellel again
        if self._enable_mp:
            time_interp_args = [(sonde_df_dict[sv].copy(), sv, self._sonde_time_res) for sv in sonde_columns]
            interp_results   = self.pool.starmap(MDF_toolkit._interp_sonde_times, time_interp_args)
        else:
            interp_results = [MDF_toolkit._interp_sonde_times(sonde_df_dict[sv].copy(), sv, self._sonde_time_res) for sv in sonde_columns]
        interped_df_dict = {} # now we have  a dictionary, with keys for sonde vars and
                              # values that are hourly launch_time vs. height dataframes
        for res in interp_results: interped_df_dict = {**interped_df_dict, **res}
        interped_time_index = interped_df_dict[sonde_columns[0]].index # every Xhour
        print("... finished interping, moving on!")
        print("... checking for gaps in sondes ")

        lunch_time_dti  = pd.DatetimeIndex(sorted_times.values)
        real_sondes_dti = lunch_time_dti.round(freq="H")
        real_inds       = [i for i,d in enumerate(interped_time_index) if d in real_sondes_dti]

        # interpolation has filled in *all* gaps, regardless of temporal distance.  if there
        # is > than 12 hour gap, that's too much, and we fill that back in with NaNs
        max_allowed_delta = pd.Timedelta('12h');
        interp_delta      = pd.Timedelta(f'{self._sonde_time_res}h')
        prev_time         = real_sondes_dti[0]
        data_gap_list     = []
        for it, s_time in enumerate(real_sondes_dti):
            if it == 0: continue
            time_gap  = s_time-prev_time
            if time_gap > max_allowed_delta: data_gap_list.append((prev_time+interp_delta, s_time-interp_delta))
            prev_time = s_time

        for start_gap, end_gap in data_gap_list:
            _warn(f"You had a gap of {str(end_gap-start_gap)} in your sonde data between:\n" \
                 +f"{start_gap} and {end_gap}\n is this intentional/known?")
            delete_range = pd.date_range(start_gap, end_gap, freq=f'{self._sonde_time_res}H')
            for dtime in delete_range: # fill all indexes matching times in gaps with nans
                idel = interped_df_dict[list(interped_df_dict.keys())[0]].index.get_loc(dtime)
                for sv in interped_df_dict.keys():
                    interped_df_dict[sv].iloc[idel] = np.nan

        print("... ordering sonde data ...")
        print("... this naive implementation slow and could surely be sped up")
        # now, second-last, recreate per-"sonde" dataframes like the data was originally
        # formatted to be written in their proper coordinates by loop on the interped frames
        interped_sonde_dfs = []
        for i_time in interped_time_index:
            sonde_df = pd.DataFrame(index=sonde_hgt_intervals)
            for sv in sonde_columns: sonde_df[sv] = interped_df_dict[sv].loc[i_time]

            try:
                real_sonde_ind    = real_sondes_dti.get_loc(i_time)
                sonde_dti         = regridded_sonde_dfs[real_sonde_ind].index
                sbt               = sonde_dti[0] # sonde_begin_time
                sonde_time_deltas = np.round((sonde_dti - sbt).total_seconds(), 2)
                sonde_df['time_since_release'] = sonde_time_deltas

            except KeyError as ke:
                not_time = pd.to_datetime(np.datetime64())
                sonde_df['time_since_release'] = not_time

            interped_sonde_dfs.append(sonde_df)

        sonde_columns.append('time_since_release') # write this out to file

        try: self._h_pbl_df
        except:
            self._h_pbl_df = _calculate_all_pbl(interped_sonde_dfs, interped_time_index)
            self._h_pbl_df['zmla_ri_grad_smoothed'] = \
                self._h_pbl_df['zmla_ri_grad'].rolling(window=20,center=False, min_periods=1).mean()
            self._h_pbl_df['zmla_temp_grad_smoothed'] = \
                self._h_pbl_df['zmla_temp_grad'].rolling(window=20,center=False, min_periods=1).mean()

        self.add_data_timeseries(self._h_pbl_df, cadence="time60")

        self._sorted_sonde_times   = sorted_times
        self._sorted_sonde_indexes = sorted_indexes
        self._interped_sonde_times = interped_time_index
        self._interped_sonde_dfs   = interped_sonde_dfs

        return interped_time_index, interped_sonde_dfs


    # ##################################################################################
    # some MDF_toolkit class helper functions that do common operations for the class
    # ##################################################################################
    def vprint(*pargs, **kargs):
        """
        vprint() calls print if the verbose flag is set by the class constructor
        """
        if isverbose: print(*pargs, **kargs)

    # toolkit specific sonde processing functions, thus static class members
    # that can't likely easily be used generically, mostly just for data re-formatting
    @staticmethod
    def _regrid_sonde_heights(sonde_df, sonde_hgt_intervals):
        """
        #documentme
        """
        if type(sonde_df.index[0]) != type(pd.Timestamp(0)):
            raise DataSpecError(f"Sonde must be indexed with timestamps")

        # convert to seconds before interpolation to avoid some weird "NaT" conversions
        sonde_df['seconds'] = [time.mktime(t.timetuple()) for t in sonde_df.index]
        sonde_df = sonde_df[~sonde_df['alt_sonde'].isnull()] # get rid of times with no alts
        sonde_df.index = sonde_df['alt_sonde'].values # we want to grid to the altitude

        # drop alt points that are duplicated because can't have multiple measures in interpolation
        sonde_df = sonde_df[~sonde_df['alt_sonde'].duplicated()]

        sonde_regridded_df =  regrid_vertical(sonde_df, sonde_hgt_intervals)

        # converting back to real timestamps from interpolated seconds you can get
        # nat(s) when there are no points, watch out for shorty-sondes
        sonde_regridded_df['time'] = [datetime.datetime.fromtimestamp(t) \
                                      if not np.isnan(t) else pd.to_datetime(np.datetime64())
                                      for t in sonde_regridded_df['seconds']]

        sonde_regridded_df.index = sonde_regridded_df['time']
        sonde_regridded_df       = sonde_regridded_df.drop(columns=['time'], axis=1)
        sonde_regridded_df       = sonde_regridded_df.drop(columns=['seconds'], axis=1)

        return sonde_regridded_df

    @staticmethod
    def _combine_sondes(sonde_df_list, sonde_hgt_intervals, var_name):

        """
        This combines a list of sonde dataframes that have *already* been normalized
        to a height grid into a *single* DataFrame for the purposess of being fed
        into a function that does time interpolation on the data.

           Parameters
            ----------
            sonde_df_list : list(pd.DataFrame, pd.DataFrame, ...)
                List of sonde profiles indexed by time of observation, but already
                normalized to even height intervals by __regrid_sonde_heights.
                It is assumed that this list is time ordered before being sent here...

            sonde_hgt_intervals : list(range(hgt_min, hgt_max, hgt_steps))
                List of the height intervals the data is normalized on

            var_name : str()
                The variable from the DataFrames in sonde_df_list that will be
                combined and returned
        """

        vprint(f"... regridding {var_name}")
        grid_df = pd.DataFrame(columns=sonde_hgt_intervals)
        for sonde_df in sonde_df_list:
            lunch_time = sonde_df.index[0].round(freq="H")
            grid_df.loc[lunch_time] = sonde_df[var_name].values

        # could put an "if index not_sorted then sort index and warn user? for
        return {var_name : grid_df}

    @staticmethod
    def _interp_sonde_times(sonde_time_height_df, name, hour_interval=1):
        print(f"... interping {name}")
        # drop alt points that are duplicated because can't have multiple measures in interpolation
        sonde_time_range = pd.date_range(sonde_time_height_df.index[0],
                                         sonde_time_height_df.index[-1],
                                         freq=f'{hour_interval}H')

        index_union     = sonde_time_height_df.index.union(sonde_time_range)
        sonde_reindexed = sonde_time_height_df.reindex(index_union)
        interped_sondes  = sonde_reindexed.interpolate('cubic')

        return { name : interped_sondes }


    @staticmethod
    def _check_and_drop_vars(data, data_id, verbose=True, strict=True, hktabledir=None, hktableversion=None):
        """

        Internal function to check DataFrame columns against the
        MDF spec and drop the variables. Also notifies the user if something
        is odd about the data that they've provided.

        Returns
        -------
        pd.DataFrame || [pd.DataFrames] with columns renamed to  MDF spec

        Parameters
        -----
        data : pd.DataFrame || [pd.DataFrames] that is supposed to adhere to MDF spec

        strict: bool, default True
            Whether to drop variables whose names cannot be identified in the
            HK table (using infermdfname).

        hktabledir: str or pathlib.Path, optional
            The base directory for the HK table files. If None (default) then
            mdftoolkit/HKTable is used.

        hktableversion: str, optional
            The table version to use. This can be a string such as "1.1", in
            which case the filenames are "[file].V1_1.json". It can also be a
            literal string like "V1_1" or "custom". If None (default) then the
            latest version available in basedir is used.

        """

        if type(data) != type([]):
            data = [data] # let people pass single frame/set
            singleton = True
        else: singleton = False

        new_data_list = []
        for d in data:
            var_defs = define_mdf_variables(
                hktabledir=hktabledir, hktableversion=hktableversion)

            if type(d) == type(xr.Dataset()):
                xarr = True
                orig_ds = d
                data_cols = list(d.variables)
            else:
                data_cols = d.columns
                xarr = False

            dropped_vars = [
                v for v in data_cols if infermdfname(v, var_defs) is None]

            if len(dropped_vars) and d is data[0]:
                print("")
                _warn("Not using these vars from dataset {}:\n {}".format(data_id, _str_list_to_str(dropped_vars)));
                print("")

            if xarr:
                if strict: d = d.drop(dropped_vars)
                for c in orig_ds.coords: d[c] = orig_ds[c] # recopy time/height coordinates

            elif strict: d = d.drop(columns=dropped_vars)

            new_data_list.append(d)

        if singleton: return new_data_list[0]
        return new_data_list

class DataSpecError(Exception):
    """

    Exception class used for MDF_toolkit, used to indicate what in
    the spec was violated and when/where. Could likely be improved.

    """
    def __init__(self, reason, func_name=inspect.stack()[1][3]):
        self._func_name = func_name
        self._reason    = reason
    def __str__(self):
        return f"You supplied incorrect data to {self._func_name} that was outside of\n"+ \
               f"the spec for {self._reason}"

# these utility functions are to keep the code clean and seperate parallelize-able
# tasks out for pool-threading, they  must be defined here outside the class
# to be picklable for pool multiprocessing... yuck

# internal object storage for data provided to the MDF toolkit these are the
# variables/data that *actually* get written to the MDF files, if it's not
# in here, it doesn't go to disk. this is an inner class, because some supersites have one site
# with data and others have more than one... this maybe should be called subsite I guess
class subsite:
    def __init__(self, cadence_options):
        self.scalar_data = {} # dict of values from user key is the MDF specced name of the scalar
        self.var_data    = {} # dict of dfs from the user, where key is cadence and value is list of DataFrames
        self.vert_data   = {}
        # initialize list of dataframes that will be stored at each cadence
        for cadence in cadence_options:
            self.var_data[cadence] = []; self.vert_data[cadence] = []


# end of MDF_toolkit class definitions
# ------------------------------------------------------------------------------------------------------------
#
# Some helper functions, likely useful to people processing typical data formats, etc, easier.
# If you write/use functions for your data ingest often, submit them as a static function in a pull request
# here to the MDF class, others might find them useful.
#
# ------------------------------------------------------------------------------------------------------------

#
#df = regrid_vertical(pd.DataFrame(ds.loc[{'time': ds.time[0]}]['Avg_Retrieved_IWC'].values, index=ds.nheights), hi)

def regrid_vertical_df(df, hgt_intervals):

    """

    Takes a dataframe with a (potentially uneven) height index, and interpolates
    it to the given grid spacing. Down here because it could be useful externally

    """
    index_union = df.index.union(hgt_intervals) # add the intervals to the df
    rdf         = df.reindex(np.float64(index_union)) # and then reindex to them
    interped_df = rdf.interpolate('cubic') # finally cubic interpolate to those heights
    interped_df = interped_df.reindex(hgt_intervals) # return only desired intervals

    return interped_df

def regrid_vertical(data, new_hgts, orig_hgts=None, iter_deets=None):

    """

    Takes data (pd.Series/np.ndarray) with a non-uniform height index, and interpolates
    it to the given grid spacing. Down here because it could be useful externally.

        Returns
        -------
        ( pd.Series || np.ndarray ) interped array on new_hgts grid

        Parameters
        -----
        data : ( pd.DataFrame || xr. || np.ndarray ) 1-d data array with height (not time!) coords/index

        new_hgts : ( np.ndarray || [] ) 1-d data array with desired grid to interpolate to

        orig_hgts : ( np.ndarray || [] ) 1-d array indicating original height grid, only required if type(data) != pd.Series

    """
    if iter_deets is not None:
        if iter_deets[0] % 10000 == 0: print(f"... interpolating {iter_deets[1].values} for var {iter_deets[2]}")

    from scipy.interpolate import griddata
    if orig_hgts is None: # given pandas object with index
        if type(data) not in [type(pd.DataFrame()), type(pd.Series())]:
            raise DataSpecError(f"must provide coordinates if not using pandas indexed series")
        orig_hgts = data.index.values
        if type(data)==type(pd.Series()): vals = data.values
    else:
        try: vals = data.values # xr.DataArray
        except: vals = data     # np.ndarray

    if type(data) == type(pd.DataFrame()):
        new_df = pd.DataFrame(index=new_hgts)
        for c in data.columns: new_df[c] = griddata(orig_hgts, data[c].values, (new_hgts), method='cubic')
        return new_df
    else:
        new_vals = griddata(orig_hgts, vals, (new_hgts), method='cubic')
        if type(data) == type(pd.Series()): return pd.Series(new_vals, index=new_hgts)
        return new_vals

def get_code_version():
    """
    Returns code version number only

    See also
    --------
    MDF_toolkit.get_person_to_blame
    """
    return MDF_toolkit.__version_info__

def get_person_to_blame():
    """
    Returns code version info in tuple w/ len==3 :
           (version_number_str, date_str, author_email_str)
    """
    return MDF_toolkit.__version_info__[2]


def df_from_ncfiles(file_locations, verbose=False):
    """

    A helper function that some people are likely to find useful.
    Takes a list of netCDF file locations and returns single pandas DataFrame
    containing all data concatted from these files sequentially.
    xarray is a dependency because it does all the hard work of
    importing for you (converting times, checking dims, etc etc).
    This function is pretty stupid and only works if your netCDF files
    are put together in a straightforward way where time is an
    infinite dimension etc. etc.

    Returns
    -------
    pandas DataFrame of concatted netCDF data

    Parameters
    ----------
    file_location(s) : str, list-like
        A list of netcdf (or other xarray supported data types)
        file locations that you would like to open.

    """

    try:
        import xarray as xr # this dependency could be removed with some added code...
    except:
        _warn("xarray is currently a dependency of this function, it contains a lot of\n"
              "useful netcdf ingesting and processing routines")
        raise

    if type(file_locations) == type('string'):
        file_locations = [file_locations]

    df_list = [ ]
    for data_file in file_locations:
        print("... opening {}".format(data_file))
        try:
            xarr_ds = xr.open_dataset(data_file)
            df_list.append(xarr_ds.to_dataframe())
        except Exception as e:
            print("!!! xarray exception: {}".format(e))
            print("!!! cannot open file {}".format(data_file))
    try:
        data_frame = pd.concat(df_list)
        data_frame = data_frame.sort_index()
        return data_frame

    except Exception as e:
        print(f"\n\nReceived exception:\n{e}")
        print("!!! cannot concat the following files, maybe something wrong with the dimensions, etc??:")
        nchars = 76
        for data_file in file_locations:
            if nchars >= 75: print("\n              ",end="");  nchars=0
            print("{},".format(data_file), end =" ", flush=True); nchars+=len(data_file)
        print("\n ... returning empty data frame")
        return pd.DataFrame() # return empty data frame


# end of MDF_toolkit(object)
# ------------------------------------------------------------------------------------------------------------
# begin generic non-class functions other people don't really need to see/use, this is why the start with _
def printline(start='',end=''): # print, a line!
    print(f"{start}--------------------------------------------------------------------------------------------{end}")

# underscore means not really intended to be used outside of this file
def _str_list_to_str(str_list):
    ret_str  = ""
    line_str = "        "
    for s in str_list:
        if len(line_str) > 65:
            ret_str = ret_str+line_str+"\n"
            line_str = "        "
        line_str = line_str+"{},".format(s)
    ret_str = ret_str+line_str.strip(",")
    return ret_str

def _warn(string): # function to make grepping lines easier, differentiating between normal output and warnings
    max_line = len(max(string.splitlines(), key=len))
    print("!!! WARNING {} !!!".format("!"*(max_line)))
    for line in string.splitlines():
        print("!!! WARNING {} {}!!! ".format(line," "*(max_line-len(line))))
    print("!!! WARNING {} !!!\n".format("!"*(max_line)))

def _ask_yn(question):
    valid  = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    while True:
        print(question)
        choice = input(" ---> ").lower()
        if choice in valid:
            return valid[choice]
        else:
            print("Please respond with 'yes' or 'no' (or 'y' or 'n').\n\n")

def _perc_missing(series):
    if series.size == 0: return 100.0
    return np.round((np.count_nonzero(np.isnan(series))/float(series.size))*100.0, decimals=4)

def _bytes_to_hr(byte_size): # humanreadable string
    power = 2**10
    n=0
    while byte_size > power: byte_size/=power; n+=1
    power_labels = {0:"", 1:"kilo", 2:"mega", 3:"giga", 4:"tera"}
    return "{} {}bytes".format(round(byte_size,4),power_labels[n])

def _bytes_to_size(bytes):
   sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
   if (bytes == 0): return '0 Byte'
   i = int(np.floor(np.log(bytes) / np.log(1024)))
   print(bytes)
   denom = np.power(1024, i)
   print(denom)
   return np.round(bytes / denom , 2) + ' ' + sizes[i]

def _create_pbl_timeseries(interped_sonde_times, interped_sonde_dfs):
    for itime, df in enumerate(interped_sonde_dfs):
        df = _calculate_pbl_height(df)


def _calculate_all_pbl(sonde_df_list, sonde_time_list):
    h_pbl_list = [_calculate_pbl_height(df, sonde_time_list[itime]) for itime, df in enumerate(sonde_df_list)]
    return pd.DataFrame(h_pbl_list, index=sonde_time_list) # combine list of dictionaries into dataframe

    # ts_vars = ['zmla_temp_grad', 'zmla_ri_grad', 'zmla_ri_bulk']
    # values_dict  = {}
    # for v in ts_vars: values_dict[v] = []
    # for pbl_dict in h_pbl_list:
    #     for v in ts_vars: values_dict[v].append(pbl_dict[v])

    # pbl_df = pd.DataFrame(index=sonde_time_list, )


# assumes units correctly match MDF spec, requires sondes with temperature
# you were here, you've calculated the interpolate heights, now the richardson number
# then you can estimate PBL height

# good paper introduction to PBL calculations, turbulence, and dynamics:
# https://link.springer.com/content/pdf/10.1023/A:1020376832738.pdf

# another comparing methods:
# https://link.springer.com/article/10.1007/s10546-014-9929-z

# from SHEBA
# https://www.tandfonline.com/doi/pdf/10.1080/16742834.2011.11446916
def _calculate_richardson_number(df):

    p0           = 100000 #pa
    epsilon      = 0.622  # molecular ratio
    svp_at_zeroc = 6.112*100 # Pa

    # calculate saturation vapor pressure so we can get saturation mixing
    # ratio, ws, for virtual potential temperature calculations, [Bolton1980]
    df['theta'] = df['ta']*(p0/df['pa'])**0.286

    svp = svp_at_zeroc * np.exp(17.67 * (df['ta'] - 273.15)/(df['ta'] - 29.65))
    q   = (epsilon * svp)/(df['pa'] - (0.378 * svp))
    ws  = epsilon * (svp/(df['pa'] - svp))
    rh  = df['hur']/100.0

    # r is the mixing ratio of water vapor, and rl is the mixing ratio of liquid water in the air.
    r = (rh)*(ws)

    df['thetavirt'] = df['theta'] * ((r + epsilon)/(epsilon * (1 + r)))
    df['tempvirt']  = df['ta'] * ((r + epsilon)/(epsilon * (1 + r)))

    g =  9.8 # m/s**2
    beta = (g/df['tempvirt'])

    d_z = pd.Series(df.index).diff().values # take values because index is wonky
    d_thetav = df['thetavirt'].diff()
    d_u = df['ua'].diff()
    d_v = df['va'].diff()
    df['ri_grad'] = (beta*(d_thetav/d_z)) \
                   /( (d_u/d_z)**2 + (d_v/d_z)**2 )

    wind_vel = np.sqrt(np.square(df['ua']) + np.square(df['va']))
    df['ri_bulk'] = (beta*d_thetav*df.index.values)/(wind_vel**2)

    return df

def _calculate_pbl_height(df, lunch_time):

    pbl_height_values = {}

    try: df['tempvirt']
    except KeyError: df = _calculate_richardson_number(df)

    zmla_ri_crit = 0.25

    #df['ri'] = df['zmla_ri_bulk'].rolling(window=10,center=False).mean()
    df['ri_grad'] = df['ri_grad'].rolling(window=10,center=False, min_periods=1).mean()
    df['ri_bulk'] = df['ri_bulk'].rolling(window=10,center=False, min_periods=1).mean()

    pbl_height_values['zmla_ri_grad'] = df['ri_grad'][0:-1].gt(zmla_ri_crit).idxmax()
    pbl_height_values['zmla_ri_bulk'] = df['ri_bulk'][0:-1].gt(zmla_ri_crit).idxmax()

    im_grad = df['ri_grad'].index.get_loc(pbl_height_values['zmla_ri_grad'])
    im_bulk = df['ri_bulk'].index.get_loc(pbl_height_values['zmla_ri_bulk'])

    tot_over_grad = (df['ri_grad'][0:-1] > zmla_ri_crit).sum()
    tot_over_bulk = (df['ri_bulk'][0:-1] > zmla_ri_crit).sum()

    if tot_over_bulk == 0: pbl_height_values['zmla_ri_bulk'] = np.nan
    if tot_over_grad == 0: pbl_height_values['zmla_ri_grad'] = np.nan

    #if pbl_height_values['zmla_ri_bulk'] > 1500: dm(locals(),0)

    # To generalize from the three SBL cases, the SBL top is defined as the
    # height where ∂θv/∂z first becomes smaller than the specified threshold
    # and above the local maximum, if it exists. Following this definition, h
    # will be at the level of maximum ∂θv/∂z when the observed ∂θv/∂z at all
    # levels is less than the given detection threshold.
    d_z      = pd.Series(df.index).diff().values # take values because index is wonky
    d_thetav = df['thetavirt'].diff()
    dth_dz   = ((d_thetav/d_z)*1000).rolling(window=10, center=False, min_periods=1).mean() #(per km)

    dtheta_crit = 40 # somewhere between 40 and 80 K/km?
    #  at all levels is less than the given detection threshold.
    try: pbl_height_values['zmla_temp_grad'] = dth_dz.loc[0:8000][dth_dz.gt(40)].index[-1]
    except IndexError: pbl_height_values['zmla_temp_grad'] = np.nan

    avg_all     = np.mean(list(pbl_height_values.values()))
    scheme_diff = tot_over_bulk-tot_over_grad

    #print(f"Height of BL on {lunch_time.strftime('%j')}: {avg_all}")
    #if scheme_diff  > 100: print(f"... disagree by {scheme_diff}")

    return pbl_height_values # dictionary
