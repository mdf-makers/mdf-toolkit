# Changelog for major revisions of the tower processing code

## Revision: 0.1, 8/20/2020, M. Gallagher
Creating the code foundation. Hard to comment on everything. Should have probably done more minor commits along the way, it's been a ramshackle path. But! Here we are. It's the great siteMIP data beta. Hope other people find it useful.

