# Installation instructions

To install the toolkit as a package, first clone the repository using

    git clone https://gitlab.com/mdf-makers/mdf-toolkit.git

for HTTPS or

    git clone git@gitlab.com:mdf-makers/mdf-toolkit.git

for SSH. Then from the `mdf-toolkit` directory run either of the following:

    python setup.py [develop]
    pip install [--user] [-e] .

The `develop` option with `setup.py` and the `-e` option for `pip` are useful for developers; they allow the current code, including all changes since installation, to always be imported. This makes it easier to work with the code as it's being developed. The `--user` option on `pip` is for a local installation, including cases where you do not have permission to install packages globally (such as on a server). The `pip` option will also attempt to install any modules from `requirements.txt` that are missing.

After installation, the primary toolkit can be imported with

    from mdftoolkit import MDF_toolkit

This is currently written as a namespace package, but future versions may add an `__init__.py` so that all `MDF_toolkit` functions are imported with `mdftoolkit` itself. There are also plans to add the checking utilities as an independent namespace package with command-line interface.
